function validate(password) {
    var lowerText = new RegExp("[a-z]+");
    var numbers = new RegExp("[0-9]");
    var upperText = new RegExp("[A-Z]");

    var passwordObj = $("#hard-password");

    if (password.length == 0) {
        $("#hard-password").ready(function () {
            passwordObj.css("display", "none");
        });
    }
    if (lowerText.test(password) || numbers.test(password) || upperText.test(password)) {
        $("#hard-password").ready(function () {
            passwordObj.css({"background-color": "grey", "display": "inherit"});
            passwordObj.text("Low password");
        });
    }
    if ((numbers.test(password) && lowerText.test(password)
        || numbers.test(password) && upperText.test(password) ||
        upperText.test(password) && lowerText.test(password))
        && password.length > 10) {
        $("#hard-password").ready(function () {
            passwordObj.css({"background-color": "orange", "display": "inherit"});
            passwordObj.text("medium password");
        });
    }
    if (numbers.test(password) && lowerText.test(password) && upperText.test(password) && password.length > 15) {
        $("#hard-password").ready(function () {
            passwordObj.css({"background-color": "green", "display": "inherit"});
            passwordObj.text("good password");
        });
    }
}

function pointValid(point, object) {
    point.trim();
    var lowerText = new RegExp("^(?![ЪъЬьЫы-])[A-ZА-Я]+[' \\-A-Za-zА-ЯЪҐЇІЁа-яїґіъё]{1,35}[^!-@_]$");

    var imgObj = $(object);

    if(point.length == 0){
        imgObj.ready(function () {
            imgObj.attr({src:"/summarytask4/img/none.png"});
        });
     }else {
        if (lowerText.test(point)) {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/checked.png"});
            });
        } else {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/cross.png"});
            });
        }
    }
}

function digitValid(point, object) {
    var lowerText = new RegExp("^[1-9][0-9]{0,6}[0-9]$");

    var imgObj = $(object);

    if(point.length == 0){
        imgObj.ready(function () {
            imgObj.attr({src:"/summarytask4/img/none.png"});
        });
    }else {
        if (lowerText.test(point)) {
            imgObj.ready(function () {
                console.log("Match!");
                imgObj.attr({src: "/summarytask4/img/checked.png"});
            });
        } else {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/cross.png"});
            });
        }
    }
}

function nameValid(point, object) {
    point.trim();
    var lowerText = new RegExp("^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \\-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}");

    var imgObj = $(object);

    if(point.length == 0){
        imgObj.ready(function () {
            imgObj.attr({src:"/summarytask4/img/none.png"});
        });
    }else {
        if (lowerText.test(point)) {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/checked.png"});
            });
        } else {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/cross.png"});
            });
        }
    }
}

function irrDigitValid(point, object) {
    var lowerText = new RegExp("^[0-9]+(\\.|,)?[0-9]{1,6}[0-9]$");

    var imgObj = $(object);

    if(point.length == 0){
        imgObj.ready(function () {
            imgObj.attr({src:"/summarytask4/img/none.png"});
        });
    }else {
        if (lowerText.test(point)) {
            imgObj.ready(function () {
                console.log("Match!");
                imgObj.attr({src: "/summarytask4/img/checked.png"});
            });
        } else {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/cross.png"});
            });
        }
    }
}

function loginValid(point, object) {
    var lowerText = new RegExp("^(?![!-@])(?!.*[_.]{2})[A-Za-z._[0-9]{5,100}[^!-/:-@]$");

    var imgObj = $(object);

    if(point.length == 0){
        imgObj.ready(function () {
            imgObj.attr({src:"/summarytask4/img/none.png"});
        });
    }else {
        if (lowerText.test(point)) {
            imgObj.ready(function () {
                console.log("Match!");
                imgObj.attr({src: "/summarytask4/img/checked.png"});
            });
        } else {
            imgObj.ready(function () {
                imgObj.attr({src: "/summarytask4/img/cross.png"});
            });
        }
    }
}


