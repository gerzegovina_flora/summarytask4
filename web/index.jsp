<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="title.index"/></title>
    <link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
<div class="panel">
    <%@ include file="/jsp/fragments/changeLanguage.jspf" %>
</div>
<div class="center">
    <h2 style="margin-top: 2px">Let's Work!</h2>

    <form method="post" action="${pageContext.request.contextPath}/Authentication">
        <div>
            <p><input type="text" name="login" required placeholder="<fmt:message key="welcome.login"/>" size="25"/>
            </p>

            <p><input type="password" name="password" required placeholder="<fmt:message key="welcome.password"/>"
                      size="25"/></p>
        </div>
        <div>
            <div style="float: right; margin-right: 8px">
                <input type="submit" value="<fmt:message key="welcome.submit"/>"/>
            </div>
            <div style="float: left">
                <input type="checkbox" name="save"/><fmt:message key="action.saveMe"/>
            </div>
        </div>
        <div>
        </div>
    </form>
</div>
<c:if test="${param.fail eq 'true'}">
    <div class="success-fail" style="position: fixed;top: 68%;left: 40%;width:18.2%"><img
            src="${pageContext.request.contextPath}/img/cross.png" height="15" width="15">&nbsp;&nbsp;<fmt:message
            key="user.message.notFound.user"/>!</div>
</c:if>
<c:if test="${param.auth eq 'true'}">
    <div class="success-fail" style="position: fixed;top: 68%;left: 40%;width:18.2%"><img
            src="${pageContext.request.contextPath}/img/cross.png" height="15" width="15">&nbsp;&nbsp;<fmt:message key="message.not.logged"/>!</div>
</c:if>
</body>
</html>
