<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.flight.page"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/scripts/script.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="fragments/footer.jspf" %>
    <div>
        <%@ include file="fragments/right_menu.jspf" %>
        <div class="header-list">
            <%@ include file="/jsp/fragments/success-fail.jspf" %>
            <c:if test="${sessionScope.user.idRole eq 0 or sessionScope.user.idRole eq 1}">
                <%@include file="fragments/sort_panel.jspf" %>
            </c:if>
        </div>
        <div class="header-list">
            <div class="elements">
                <div class="id">№</div>
                <div class="point"><b><fmt:message key="flight.from"/></b></div>
                <div class="point"><b><fmt:message key="flight.to"/></b></div>
                <div class="date"><b><fmt:message key="flight.date"/></b></div>
                <div class="status"><b><fmt:message key="flight.status"/></b></div>
                <div class="info-bar">
                    <div class="link-button-list">&nbsp;</div>
                </div>
                <div class="edit-bar">
                    <div class="link-button-list">&nbsp;</div>
                </div>
                <div class="delete-bar">
                    <div class="link-button-list">&nbsp;</div>
                </div>
                <c:if test="${sessionScope.user.idRole eq 0 or sessionScope.user.idRole eq 1}">
                    <div class="check-bar">
                        <div class="check-bar">
                            <div class="link-button-list"><input type="checkbox" id="selectAll"></div>
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
        <form action="${pageContext.request.contextPath}/Flight" method="post" name="myForm">
            <input type="hidden" name="command" value="dropFlights"/>
            <div class="list">
                <%@ include file="fragments/flight_list.jspf" %>
            </div>
        </form>
    </div>
</div>
<script>
    $(".deleteBox").change(function () {
        if ($(".deleteBox:checked").length) {
            $("#deletePanel").css("display", "inline");
        } else {
            $("#deletePanel").css("display", "none");
        }
    });

    var state = false;
    $("#selectAll").change(function () {
        $("#selectAll").each(function () {
            if (!state) {
                if ($(".deleteBox").prop("checked", true)) {
                    state = true;
                    $("#deletePanel").css("display", "inline");
                }
            } else {
                if ($(".deleteBox").prop("checked", false)) {
                    state = false;
                    $("#deletePanel").css("display", "none");
                }
            }
        });
    });
</script>
</body>
</html>
