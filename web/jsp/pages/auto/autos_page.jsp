<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="title.auto.page"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="header-list" style="height: 20px;">
            <div style="display: inline; float: right;">
                <div id="deletePanel" style="display: none"><a
                        href="javascript:document.myForm.submit()"
                        onclick="return confirm('<fmt:message key="message.sure.delete.autos"/>')"><fmt:message key="message.delete.choice"/></a></div>
            </div>
        </div>
        <form action="${pageContext.request.contextPath}/Auto" method="post" name="myForm">
            <div class="header-list">
                <%@ include file="/jsp/fragments/success-fail.jspf" %>
                <input type="hidden" name="command" value="dropAutos"/>
                <div class="elements">
                    <div class="name"><b><fmt:message key="auto.name"/></b></div>
                    <div class="type"><b><fmt:message key="auto.type"/></b></div>
                    <div class="weight"><b><fmt:message key="auto.weight"/>, <fmt:message key="auto.measure"/></b></div>
                    <div class="count-place"><b><fmt:message key="auto.countPlace"/></b></div>
                    <div class="state"><b><fmt:message key="auto.state"/></b></div>
                    <div class="weight-capacity"><b><fmt:message key="auto.weight.capacity"/></b></div>
                    <div class="engine"><b><fmt:message key="auto.engine"/></b></div>
                    <div class="gear"><b><fmt:message key="auto.gear"/></b></div>
                    <div class="edit-bar">&nbsp;</div>
                    <div class="delete-bar">&nbsp;</div>
                    <div class="check-bar"><div class="link-button-list"><input type="checkbox" id="selectAll"></div></div>
                </div>
            </div>
            <div class="list">
                <c:choose>
                    <c:when test="${not empty applicationScope.autos}">
                        <c:forEach items="${applicationScope.autos}" var="auto">
                            <div class="elements">
                                <c:url var="removeUrl" value="/Auto?id=${auto.id}&command=deleteAuto"/>
                                <c:url var="editUrl"
                                       value="/jsp/pages/auto/edit_auto_page.jsp?id=${auto.id}&state=${auto.autoState}&type=${auto.characteristic.autoType}&weight=${auto.characteristic.weight}&count=${auto.characteristic.placeCount}&name=${auto.name}&weightCapacity=${auto.characteristic.weightCapacity}&engine=${auto.characteristic.engineType}&gear=${auto.characteristic.gearType}"/>

                                <div class="name"><c:out value="${auto.name}"/></div>
                                <div class="type"><fmt:message key="auto.type.${fn:toLowerCase(fn:replace(auto.characteristic.autoType, '_',''))}"/></div>
                                <div class="weight"><c:out value="${auto.characteristic.weight}"/></div>
                                <div class="count-place"><c:out value="${auto.characteristic.placeCount}"/></div>
                                <div class="state"><fmt:message key="auto.state.${fn:toLowerCase(auto.autoState)}"/></div>
                                <div class="weight-capacity"><c:out value="${auto.characteristic.weightCapacity}"/></div>
                                <div class="engine"><fmt:message key="auto.engine.type.${fn:toLowerCase(auto.characteristic.engineType)}"/></div>
                                <div class="gear"><fmt:message key="auto.gear.type.${fn:toLowerCase(fn:replace(auto.characteristic.gearType, '_',''))}"/></div>

                                <div class="edit-bar"><div class="link-button-list"><a href="${editUrl}"><fmt:message key="action.edit"/></a></div>
                                </div>
                                <div class="delete-bar"><div class="link-button-list"><a href="${removeUrl}" onclick="return confirm('<fmt:message key="message.sure.delete.auto"/>')"><fmt:message
                                        key="action.delete"/></a></div>
                                </div>
                                <div class="check-bar"><div class="link-button-list"><input type="checkbox" class="deleteBox"
                                                                     name="checkedAuto"
                                                                     value="${auto.id}"/></div></div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <div class="elements">
                            <fmt:message key="auto.message.empty.auto"/>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </form>
    </div>
</div>
<script>
    $(".deleteBox").change(function () {
        if ($(".deleteBox:checked").length) {
            $("#deletePanel").css("display", "inline");
        } else {
            $("#deletePanel").css("display", "none");
        }
    });

    var state = false;
    $("#selectAll").change(function () {
        $("#selectAll").each(function () {
            if (!state) {
                if ($(".deleteBox").prop("checked", true)) {
                    state = true;
                    $("#deletePanel").css("display", "inline");
                }
            } else {
                if ($(".deleteBox").prop("checked", false)) {
                    state = false;
                    $("#deletePanel").css("display", "none");
                }
            }
        });
    });
</script>
</body>
</html>
