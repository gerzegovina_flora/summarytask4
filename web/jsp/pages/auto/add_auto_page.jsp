<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="own" uri="http://owntag.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.auto.add"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/scripts/script.js" type="text/javascript"></script>
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="fill">
            <form action="${pageContext.request.contextPath}/Auto" method="post">
                <input type="hidden" name="command" value="createAuto"/>
                <pre><fmt:message key="auto.weight"/>, <fmt:message key="auto.measure"/>:&#9;<input id="weight-form" type="text" name="weight" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="weight-img" height="20" width="20" /><c:if test="${requestScope.weightFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.countPlace"/>:&#9;<input type="text" name="count_place" id="count-form" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="count-img" height="20" width="20" /><c:if test="${requestScope.placeCountFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.weight.capacity"/>:<input type="text" name="weightCapacity" id="weight-capacity-form" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="capacity-img" height="20" width="20" /><c:if test="${requestScope.weightCapacity}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.name"/>:&#9;<input type="text" name="name" required id="name-form"/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="name-img" height="20" width="20" /><c:if test="${requestScope.nameFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.type"/>:&#9;&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType"
                                                               name="car_type" localePrefix="auto.type"/></pre>
                <pre><fmt:message key="auto.engine"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType" parameter="${param.engine}"
                                                                         name="engine" localePrefix="auto.engine.type"/></pre>
                <pre><fmt:message key="auto.gear"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.GearType" parameter="${param.gear}"
                                                                       name="gear" localePrefix="auto.gear.type"/></pre>
                <pre><fmt:message key="auto.state"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState" parameter="${param.state}"
                                                                name="state" localePrefix="auto.state"/></pre>
                <input type="submit" value='<fmt:message key="action.add"/>'/><br/>
                <%@ include file="/jsp/fragments/success-true.jspf"%>
                <%@ include file="/jsp/fragments/success-fail.jspf"%>
            </form>
        </div>
    </div>
</div>
<script>
    $("#weight-form").keyup(function () {
        var weight = $("#weight-form").val();
        irrDigitValid(weight,"#weight-img");
    });
    $("#count-form").keyup(function () {
        var point = $("#count-form").val();
        digitValid(point,"#count-img");
    });
    $("#weight-capacity-form").keyup(function () {
        var point = $("#weight-capacity-form").val();
        irrDigitValid(point,"#capacity-img");
    });
    $("#name-form").keyup(function () {
        var point = $("#name-form").val();
        nameValid(point,"#name-img");
    });
</script>
</body>
</html>
