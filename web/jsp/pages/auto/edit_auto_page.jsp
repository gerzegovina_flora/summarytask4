<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="own" uri="http://owntag.pivovarenko.nure.ua" %>
<html>
<head>
    <title><fmt:message key="title.auto.edit"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/scripts/script.js" type="text/javascript"></script>
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
            <div class="fill">
            <form method="post" action="${pageContext.request.contextPath}/Auto">
                <input type="hidden" name="command" value="editAuto"/>
                <input type="hidden" name="id" value="${param.id}"/>
                <pre><fmt:message key="auto.weight"/>:&#9;&#9;  <input id="weight-form" type="text" name="weight" value="${param.weight}" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="weight-img" height="20" width="20" /><c:if test="${requestScope.weightFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.countPlace"/>:&#9;  <input id="count-form" type="text" name="count" value="${param.count}" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="count-img" height="20" width="20" /><c:if test="${requestScope.placeCountFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.weight.capacity"/>:  <input id="weight-capacity-form" type="text" name="weightCapacity" value="${param.weightCapacity}" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="capacity-img" height="20" width="20" /><c:if test="${requestScope.weightCapacity}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.name"/>:&#9;  <input id="name-form" type="text" name="name" value="${param.name}" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="name-img" height="20" width="20" /><c:if test="${requestScope.nameFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="auto.type"/>:&#9;&#9;  <own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType"
                                                               parameter="${param.type}" localePrefix="auto.type"
                                                               name="type"/></pre>
                <pre><fmt:message key="auto.engine"/>&#9;  <own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType" parameter="${param.engine}"
                                                                         name="engine" localePrefix="auto.engine.type"/></pre>
                <pre><fmt:message key="auto.gear"/>&#9;  <own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.GearType" parameter="${param.gear}"
                                                                       name="gear" localePrefix="auto.gear.type"/></pre>
                <pre><fmt:message key="auto.state"/>:&#9;  <own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState"
                                                                parameter="${param.state}" localePrefix="auto.state"
                                                                name="state"/></pre>
                <input type="submit" value="<fmt:message key="action.accept"/>"/>
                <%@ include file="/jsp/fragments/success-fail.jspf"%>
            </form>
        </div>
    </div>
</div>
<script>
    $("#weight-form").keyup(function () {
        var weight = $("#weight-form").val();
        irrDigitValid(weight,"#weight-img");
    });
    $("#count-form").keyup(function () {
        var point = $("#count-form").val();
        digitValid(point,"#count-img");
    });
    $("#weight-capacity-form").keyup(function () {
        var point = $("#weight-capacity-form").val();
        irrDigitValid(point,"#capacity-img");
    });
    $("#name-form").keyup(function () {
        var point = $("#name-form").val();
        nameValid(point,"#name-img");
    });
</script>
</body>
</html>
