<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.auto.accept"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="info">
            <form method="post" action="${pageContext.request.contextPath}/Statement">

                <input type="hidden" name="idStatement" value="${requestScope.idStatement}"/>
                <input type="hidden" name="idFlight" value="${requestScope.idFlight}"/>
                <input type="hidden" name="command" value="acceptStatement"/>

                <fmt:message key="auto.message.choose.auto"/>:<br/><br/>

                <select name="foundCarId">
                    <c:choose>
                        <c:when test="${not empty reqCar}">
                            <c:forEach items="${reqCar}" var="car">
                                <option value="${car.id}">${car.name}</option>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <option value=""><fmt:message key="value.none"/></option>
                        </c:otherwise>
                    </c:choose>
                </select>

                <input type="submit" value='<fmt:message key="action.accept"/>'/>
                <%@ include file="/jsp/fragments/success-fail.jspf"%>
            </form>
        </div>
    </div>
</div>
</body>
</html>
