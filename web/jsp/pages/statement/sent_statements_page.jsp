<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.statement.sent"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<%@ include file="/jsp/fragments/footer.jspf" %>
<div>
    <%@ include file="/jsp/fragments/right_menu.jspf" %>
    <div class="header-list">
        <%@ include file="/jsp/fragments/success-fail.jspf" %>
        <div class="elements">
            <div class="point"><b><fmt:message key="flight.from"/></b></div>
            <div class="point"><b><fmt:message key="flight.to"/></b></div>
            <div class="date"><b><fmt:message key="user.login"/></b></div>
            <div class="type"><b><fmt:message key="auto.type"/></b></div>
            <div class="date"><b><fmt:message key="auto.weight"/>, <fmt:message key="auto.measure"/></b></div>
            <div class="count-place"><b><fmt:message key="auto.countPlace"/></b></div>
            <div class="count-place">&nbsp;</div>
            <div class="count-place">&nbsp;</div>
        </div>
    </div>
    <div class="list">
        <c:choose>
            <c:when test="${not empty sessionScope.sentStatements}">
                <c:forEach items="${sessionScope.sentStatements}" var="sentStatement">

                    <div class="elements">
                        <div class="point"><c:out value="${sentStatement.flight.from}"/></div>
                        <div class="point"><c:out value="${sentStatement.flight.to}"/></div>
                        <div class="date"><c:out value="${sentStatement.driver.login}"/></div>
                        <div class="type"><fmt:message
                                key="auto.type.${fn:toLowerCase(sentStatement.characteristic.autoType)}"/></div>
                        <div class="date"><c:out value="${sentStatement.characteristic.weight}"/></div>
                        <div class="count-place"><c:out value="${sentStatement.characteristic.placeCount}"/></div>
                    </div>

                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="elements">
                    <fmt:message key="statement.message.empty.statement.accepted"/>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
