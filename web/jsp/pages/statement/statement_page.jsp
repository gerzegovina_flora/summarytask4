<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.statement.page"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="header-list">
            <%@ include file="/jsp/fragments/success-fail.jspf" %>
            <div class="elements">
                <div class="point"><b><fmt:message key="flight.from"/></b></div>
                <div class="point"><b><fmt:message key="flight.to"/></b></div>
                <div class="date"><b><fmt:message key="user.login"/></b></div>
                <div class="type"><b><fmt:message key="auto.type"/></b></div>
                <div class="date"><b><fmt:message key="auto.weight"/>, <fmt:message key="auto.measure"/></b></div>
                <div class="count-place"><b><fmt:message key="auto.countPlace"/></b></div>
                <div class="count-place">&nbsp;</div>
                <div class="count-place">&nbsp;</div>
            </div>
        </div>
        <div class="list">
            <c:choose>
                <c:when test="${not empty applicationScope.statements}">
                    <c:forEach items="${applicationScope.statements}" var="activeStatement">
                        <div class="elements">
                            <c:url var="findCar"
                                   value="/Auto?idStatement=${activeStatement.id}&idFlight=${activeStatement.flight.id}&autoType=${activeStatement.characteristic.autoType}&weight=${activeStatement.characteristic.weight}&placeCount=${activeStatement.characteristic.placeCount}&weightCapacity=${activeStatement.characteristic.weightCapacity}&engine=${activeStatement.characteristic.engineType}&gear=${activeStatement.characteristic.gearType}&command=findCar"/>
                            <c:url var="cancelUrl"
                                   value="/Statement?id=${activeStatement.id}&idCharacteristic=${activeStatement.characteristic.id}&command=cancelStatement"/>

                            <div class="point"><c:out value="${activeStatement.flight.from}"/></div>
                            <div class="point"><c:out value="${activeStatement.flight.to}"/></div>
                            <div class="date"><c:out value="${activeStatement.driver.login}"/></div>
                            <div class="type"><fmt:message key="auto.type.${fn:toLowerCase(fn:replace(activeStatement.characteristic.autoType,'_',''))}"/></div>
                            <div class="date"   ><c:out value="${activeStatement.characteristic.weight}"/></div>
                            <div class="count-place"><c:out value="${activeStatement.characteristic.placeCount}"/></div>

                            <div class="edit-bar">
                                <div class="link-button-list"><a href="${findCar}"><fmt:message
                                        key="action.accept"/></a>
                                </div>
                            </div>
                            <div class="edit-bar">
                                <div class="link-button-list">
                                    <a href="${cancelUrl}" onclick="return confirm(<fmt:message key="message.sure.delete.statement"/>)"><fmt:message key="action.cancel"/></a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="elements">
                        <fmt:message key="statement.message.empty.statement"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
