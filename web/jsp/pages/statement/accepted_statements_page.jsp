<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.statement.accepted"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<%@ include file="/jsp/fragments/footer.jspf" %>
<div>
    <%@ include file="/jsp/fragments/right_menu.jspf" %>
    <div class="header-list">
        <%@ include file="/jsp/fragments/success-fail.jspf" %>
        <input type="hidden" name="command" value="dropAutos"/>
    </div>
    <div class="list">
        <c:choose>
            <c:when test="${not empty applicationScope.acceptedStatements}">
                <c:forEach items="${applicationScope.acceptedStatements}" var="acceptedStatement">
                    <div class="elements">
                        <c:url var="cancelUrl"
                               value="/Statement?id=${acceptedStatement.id}&idCharacteristic=${acceptedStatement.characteristic.id}&command=cancelStatement"/>
                        <b><fmt:message key="flight.from"/>:</b> <c:out value="${acceptedStatement.flight.from}"/><br/>
                        <b><fmt:message key="flight.to"/>:</b> <c:out value="${acceptedStatement.flight.to}"/><br/>
                        <b><fmt:message key="user.role.driver"/>:</b> <c:out
                            value="${acceptedStatement.driver.login}"/><br/>
                        <a href="${cancelUrl}"><fmt:message key="action.cancel"/></a>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="elements">
                    <fmt:message key="statement.message.empty.statement.accepted"/>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
