<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.flight.info"/></title>
    <link rel="stylesheet" type="text/css" href="style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="info">
            <pre><b>#&#9;</b><c:out value="${param.id}"/></pre>
            <pre><b><fmt:message key="flight.from"/>&#9;</b><c:out value="${requestScope.from}"/></pre>
            <pre><b><fmt:message key="flight.to"/>&#9;</b><c:out value="${requestScope.to}"/><br/></pre>
            <pre><b><fmt:message key="flight.date"/>&#9;</b><c:out value="${requestScope.date}"/><br/></pre>
            <pre><b><fmt:message key="flight.status"/>&#9;</b><c:out value="${requestScope.status}"/><br/></pre>
            <pre><b><fmt:message key="flight.description"/>:</b></pre>
            <c:choose>
                <c:when test="${requestScope.description eq 'null' or requestScope.description eq ''}">
                    <div class="success-fail">
                        <fmt:message key="flight.message.empty.description"/>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="description">
                        <c:out value="${requestScope.description}"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>