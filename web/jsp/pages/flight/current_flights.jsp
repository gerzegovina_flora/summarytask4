<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.flight.current"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="list">
            <c:choose>
                <c:when test="${not empty sessionScope.activeStatements}">
                    <c:forEach items="${sessionScope.activeStatements}" var="activeStatement">
                        <div class="elements">
                            <b><fmt:message key="flight.from"/>: </b><c:out
                                value="${activeStatement.flight.from}"/><br/>
                            <b> <fmt:message key="flight.to"/>: </b><c:out value="${activeStatement.flight.to}"/><br/>
                            <b><fmt:message key="auto.name"/>: </b><c:out
                                value="${activeStatement.flight.auto.name}"/><br/>

                            <a href="done_flight.jsp?idStatement=${activeStatement.id}&idFlight=${activeStatement.flight.id}&idAuto=${activeStatement.flight.auto.id}">
                                <fmt:message key="action.done"/></a>
                            <a href="${pageContext.request.contextPath}/Statement?command=cancel&idStatement=${activeStatement.id}&idFlight=${activeStatement.flight.id}">
                                <fmt:message key="action.cancel"/></a>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="elements">
                        <fmt:message key="flight.message.empty.flights.current"/>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
