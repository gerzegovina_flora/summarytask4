<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="own" uri="http://owntag.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title><fmt:message key="title.flight.done"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="fill">
            <form action="${pageContext.request.contextPath}/Statement" method="post">
                <input type="hidden" name="idStatement" value="${param.idStatement}"/>
                <input type="hidden" name="idFlight" value="${param.idFlight}"/>
                <input type="hidden" name="idAuto" value="${param.idAuto}"/>
                <input type="hidden" name="command" value="finish"/>

                <fmt:message key="message.flight.done"/>:<br/>

                <pre><fmt:message key="message.done.flight.status"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus" parameter="${param.state}"
                                               name="flightState" localePrefix="flight.status"/></pre>
                <pre><fmt:message key="message.done.auto.state"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState" parameter="${param.state}"
                                           name="autoState" localePrefix="auto.state"/></pre>
                <input type="submit" value="<fmt:message key="action.accept"/>"/>
            </form>
            <%@ include file="/jsp/fragments/success-fail.jspf"%>
        </div>
    </div>
</div>
</body>
</html>