<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="own" uri="http://owntag.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="title.flight.add"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/scripts/script.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="fill">
            <form action="${pageContext.request.contextPath}/Flight" method="post">
                <input type="hidden" value="createFlight" name="command"/>
                <pre><fmt:message key="flight.from"/>:&#9;<input id="point-from" type="text" name="from" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="point-from-img" height="20" width="20" /><c:if test="${requestScope.fromFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="flight.to"/>:&#9;<input id="point-to" type="text" name="to" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="point-to-img" height="20" width="20" /><c:if test="${requestScope.toFail}"><img src="${pageContext.request.contextPath}/img/none.png" id='point-to-img' height='20' width='20' /><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="flight.status"/>:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus"
                                                                   name="status" localePrefix="flight.status"/></pre>
                <pre><fmt:message key="flight.description"/>:</pre>
                <label for="desc">
                    <textarea id = "desc" name="description" style="resize: none" rows="10" cols="45" maxlength="255"></textarea>
                </label>
                <input type="submit" value="<fmt:message key="action.add"/>"/>
                <span id="count"></span><br/>
                <%@ include file="/jsp/fragments/success-true.jspf"%>
                <c:if test="${requestScope.duplicationFail eq true}">
                    <div class="success-fail"><img src="${pageContext.request.contextPath}/img/cross.png" height="15" width="15">&nbsp;&nbsp;<fmt:message
                            key="message.duplication"/></div>
                </c:if>
                <%@ include file="/jsp/fragments/success-fail.jspf"%>
            </form>
        </div>
    </div>
</div>
<script>
    /*Description lenght*/
    var descObj = document.getElementById("desc");
    var countObj = document.getElementById("count");
    window.onload = function loaded() {
        countObj.innerHTML = "<fmt:message key="message.leftSymbols"/>: " + (255 - descObj.value.length);
    };
    descObj.oninput = function(){
        countObj.innerHTML = "<fmt:message key="message.leftSymbols"/>: "+(255 - descObj.value.length);
    };
    /*Description from-to valid*/
    $("#point-from").keyup(function () {
        var point = $("#point-from").val();
        pointValid(point,"#point-from-img");
    });
    $("#point-to").keyup(function () {
        var point = $("#point-to").val();
        pointValid(point,"#point-to-img");
    });
</script>
</body>
</html>
