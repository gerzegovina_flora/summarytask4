<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="own" uri="http://owntag.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.user.add"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/scripts/script.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="fill">
            <form action="${pageContext.request.contextPath}/User" method="post">
                <input type="hidden" name="command" value="createUser"/>
                <pre><fmt:message key="user.login" />:&#9;<input id="login-form" type="text" name="login" required/><span class="error"><img src="${pageContext.request.contextPath}/img/none.png" id="login-img" height="20" width="20" /><c:if test="${requestScope.loginFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="user.mail" />:&#9;<input type="text" name="mail" required/><span class="error"><c:if test="${requestScope.mailFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <pre><fmt:message key="user.role" />:&#9;<own:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.core.constant.Role" name="role" localePrefix="user.role"/></pre>
                <pre><fmt:message key="user.password" />:&#9;<input id="pass" type="password" name="password" required/><span class="error"><c:if test="${requestScope.passwordFail}"><fmt:message key="value.error.input"/></c:if></span></pre>
                <div id="hard-password"><br/></div>
                <br/><input style="margin-top: 5px" type="submit" value="<fmt:message key="action.add" />"/>
                <%@ include file="/jsp/fragments/success-true.jspf"%>
                <c:if test="${requestScope.userExistFail}">
                    <div class="success-fail"><img src="${pageContext.request.contextPath}/img/cross.png" height="15" width="15">&nbsp;&nbsp;<fmt:message key="message.user.exist"/></div>
                </c:if>
                <%@ include file="/jsp/fragments/success-fail.jspf"%>
            </form>
        </div>
    </div>
</div>
<script>
    $("#hard-password").ready(function(){
        $("#hard-password").css("display", "none");
    });
    $("#pass").keyup(function () {
        var pass = $("#pass").val();
        validate(pass);
    });
    $("#login-form").keyup(function () {
        var point = $("#login-form").val();
        loginValid(point,"#login-img");
    });
</script>
</body>
</html>
