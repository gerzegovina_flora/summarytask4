<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="title.user.page"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css"/>
    <script src="${pageContext.request.contextPath}/scripts/jquery-2.1.4.min.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div>
    <%@ include file="/jsp/fragments/footer.jspf" %>
    <div>
        <%@ include file="/jsp/fragments/right_menu.jspf" %>
        <div class="header-list" style="height: 20px;">
            <%@ include file="/jsp/fragments/success-fail.jspf" %>
            <div style="display: inline; float: right;">
                <div id="deletePanel" style="display: none"><a
                        href="javascript:document.myForm.submit()"
                        onclick="return confirm('<fmt:message key="message.sure.delete.autos"/>')"><fmt:message key="message.delete.choice"/></a></div>
            </div>
        </div>
        <form action="${pageContext.request.contextPath}/User" method="post" name="myForm">
            <div class="header-list">
                <input type="hidden" name="command" value="dropUsers"/>
                <div class="elements">
                    <div class="date"><b><fmt:message key="user.login"/></b></div>
                    <div class="type"><b><fmt:message key="user.role"/></b></div>
                    <div class="edit-bar">&nbsp;</div>
                    <div class="delete-bar">&nbsp;</div>
                    <div class="check-bar"><div class="link-button-list"><input type="checkbox" id="selectAll"></div></div><br/>
                </div>
            </div>
            <div class="list">
                <c:choose>
                    <c:when test="${not empty applicationScope.users}">
                        <c:forEach items="${applicationScope.users}" var="user">
                            <div class="elements">
                                <c:url var="editUrl"
                                       value="/jsp/pages/user/edit_user.jsp?id=${user.id}&login=${user.login}&role=${user.role}&mail=${user.mail}"/>
                                <c:url var="removeUrl" value="/User?id=${user.id}&command=deleteUser"/>

                                <div class="date"><c:out value="${user.login}"/></div>
                                <div class="type"><fmt:message key="user.role.${fn:toLowerCase(user.role)}"/></div>

                                <div class="edit-bar"><div class="link-button-list"><a href="${editUrl}"><fmt:message key="action.edit"/></a>
                                </div></div>
                                <div class="delete-bar"><div class="link-button-list"><a href="${removeUrl}"
                                                                 onclick="return confirm('<fmt:message key="message.sure.delete.user"/>')"><fmt:message
                                        key="action.delete"/></a></div>
                                </div>
                                <div class="check-bar"><div class="link-button-list"><input type="checkbox" class="deleteBox"
                                                                     name="checkedUser"
                                                                     value="${user.id}"/></div></div>
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <div class="elements">
                            <fmt:message key="user.message.empty.user"/>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </form>
    </div>
</div>
<script>
    $(".deleteBox").change(function () {
        if ($(".deleteBox:checked").length) {
            $("#deletePanel").css("display", "inline");
        } else {
            $("#deletePanel").css("display", "none");
        }
    });

    var state = false;
    $("#selectAll").change(function () {
        $("#selectAll").each(function () {
            if (!state) {
                if ($(".deleteBox").prop("checked", true)) {
                    state = true;
                    $("#deletePanel").css("display", "inline");
                }
            } else {
                if ($(".deleteBox").prop("checked", false)) {
                    state = false;
                    $("#deletePanel").css("display", "none");
                }
            }
        });
    });
</script>
</body>
</html>
