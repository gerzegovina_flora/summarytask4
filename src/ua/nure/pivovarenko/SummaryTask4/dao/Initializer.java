package ua.nure.pivovarenko.SummaryTask4.dao;

import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.ServletContext;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Initialize global data
 */
public final class Initializer {

    private Initializer() {
    }

    public static void initializeAutos(ServletContext servletContext) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        AutoDAO autoDao = abstractDAO.getAutoDao();
        List<Auto> autos = autoDao.getAll();
        servletContext.setAttribute("autos", autos);
    }

    public static void initializeUsers(ServletContext servletContext) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        UserDAO userDao = abstractDAO.getUserDao();
        List<User> users = userDao.getAll();
        servletContext.setAttribute("users", users);
    }

    public static void initializeStatements(ServletContext servletContext) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        StatementDAO statementDAO = abstractDAO.getStatementDao();
        List<Statement> statements = statementDAO.getAll();
        servletContext.setAttribute("statements", statements);
    }

    public static void initializeAcceptedStatements(ServletContext servletContext) {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        StatementDAO statementDAO = abstractDAO.getStatementDao();
        List<Statement> acceptedStatements = statementDAO.getAllAcceptedStatements();
        servletContext.setAttribute("acceptedStatements", acceptedStatements);
    }

    public static void initializeAll(ServletContext servletContext) {
        initializeAutos(servletContext);
        initializeAcceptedStatements(servletContext);
        initializeStatements(servletContext);
        initializeUsers(servletContext);
    }
}