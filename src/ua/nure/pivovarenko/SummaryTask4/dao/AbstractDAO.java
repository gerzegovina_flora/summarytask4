package ua.nure.pivovarenko.SummaryTask4.dao;

import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDaoImpl;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Resource(name = "jdbc/testDB", type=javax.sql.DataSource.class)
@Stateless
public final class AbstractDAO {

    private DataSource dataSource;
    private static AbstractDAO abstractDAO = null;

    private AbstractDAO() throws NamingException {
        Context ctx = new InitialContext();
        dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/testDB");
    }

    public static synchronized AbstractDAO getInstance() {
        if (abstractDAO == null) {
            try {
                abstractDAO = new AbstractDAO();
            } catch (NamingException e) {
                System.out.println(e);
            }
        }
        return abstractDAO;
    }

    public AutoDAO getAutoDao() {
        return new AutoDaoImpl(dataSource);
    }

    public FlightDAO getFlightDao() {
        return new FlightDaoImpl(dataSource);
    }

    public StatementDAO getStatementDao() {
        return new StatementDaoImpl(dataSource);
    }

    public UserDAO getUserDao() {
        return new UserDaoImpl(dataSource);
    }

}
