package ua.nure.pivovarenko.SummaryTask4.web.filter;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Checking for Driver role
 */
public class DriverSecurityFilter extends GenericFilter<Class<DriverSecurityFilter>> {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        if (session.getAttribute("user") != null && ((User) session.getAttribute("user")).getIdRole() == 2) {
            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            FlightDAO flightDao = abstractDAO.getFlightDao();
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            long idUser = ((User) request.getSession().getAttribute("user")).getId();
            log.debug("User is a DRIVER");
            List<Flight> flights = flightDao.getAllFreeFlights(idUser);
            log.debug("DRIVER: Loading FREE Flights loaded");
            request.getSession().setAttribute("flights", flights);

            StatementDAO statementDao = abstractDAO.getStatementDao();
            log.debug("DRIVER: Loading ACTIVE Statements");
            List<Statement> activeStatements = statementDao.getAllActiveStatements(idUser);
            log.debug("DRIVER: Loading ACTIVE Statements loaded");
            request.getSession().setAttribute("activeStatements", activeStatements);

            List<Statement> sentStatements = statementDao.getSentStatements(idUser);
            log.debug("DRIVER: Loading SENT Statements loaded");
            request.getSession().setAttribute("sentStatements", sentStatements);
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            log.warn("User is not DRIVER");
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect(Path.INDEX_PAGE + "?" + Path.AUTH_FALSE);
        }
    }

    @Override
    public void destroy() {
    }
}
