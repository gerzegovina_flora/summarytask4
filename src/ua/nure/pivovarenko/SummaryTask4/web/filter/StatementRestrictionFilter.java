package ua.nure.pivovarenko.SummaryTask4.web.filter;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pivovarenko D.
 * Checking for statement restriction access
 */
@WebFilter("/jsp/pages/statement/*")
public class StatementRestrictionFilter extends GenericFilter<Class<StatementRestrictionFilter>> {

    private Map<Integer, ArrayList<String>> rolesPages = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        final String path = filterConfig.getServletContext().getContextPath();
        rolesPages = new HashMap<>();
        rolesPages.put(0, new ArrayList<String>() {{
            add(path + Path.STATEMENT_PAGE);
            add(path + Path.ACCEPTED_STATEMENT_PAGE);
        }});
        rolesPages.put(1, new ArrayList<String>() {{
            add(path + Path.STATEMENT_PAGE);
            add(path + Path.ACCEPTED_STATEMENT_PAGE);
        }});
        rolesPages.put(2, new ArrayList<String>() {{
            add(path + Path.ADD_STATEMENT_PAGE);
            add(path + Path.SENT_STATEMENT_PAGE);
        }});
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String uri = httpServletRequest.getRequestURI();
        int idRole = ((User) httpServletRequest.getSession().getAttribute("user")).getIdRole();
        if (rolesPages.get(idRole).contains(uri)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            log.warn("Bad link: STATEMENT link is incorrect");
            HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + Path.ERROR_PAGE);
        }
    }

    @Override
    public void destroy() {
    }
}
