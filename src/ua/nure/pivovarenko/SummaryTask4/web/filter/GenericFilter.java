package ua.nure.pivovarenko.SummaryTask4.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.Filter;

public abstract class GenericFilter <T extends Class<?>> implements Filter {
    protected final Logger log = Logger.getLogger((T) Class.class);
}
