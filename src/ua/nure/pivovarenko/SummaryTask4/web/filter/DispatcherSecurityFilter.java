package ua.nure.pivovarenko.SummaryTask4.web.filter;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Checking for Dispatcher role
 */
public class DispatcherSecurityFilter extends GenericFilter<Class<DispatcherSecurityFilter>> {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        if (session.getAttribute("user") != null && ((User) session.getAttribute("user")).getIdRole() == 1) {
            log.debug("User is a DISPATCHER");
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            log.warn("User is not a DISPATCHER");
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect(Path.INDEX_PAGE + "?" + Path.AUTH_FALSE);
        }
    }

    @Override
    public void destroy() {

    }
}
