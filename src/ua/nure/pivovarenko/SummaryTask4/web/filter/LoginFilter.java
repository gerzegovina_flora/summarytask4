package ua.nure.pivovarenko.SummaryTask4.web.filter;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Remember user feature
 */
@WebFilter("/index.jsp")
public class LoginFilter extends GenericFilter<Class<LoginFilter>> {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getSession().getAttribute("save") != null && (boolean) request.getSession().getAttribute("save")) {
            log.info("The user has been remembered");
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}