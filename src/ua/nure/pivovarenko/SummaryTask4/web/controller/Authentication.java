package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pivovarenko D.
 * Authentication user
 */
@WebServlet("/Authentication")
public class Authentication extends GenericServlet<Class<Authentication>> {

    private final Map<Integer, String> page = new HashMap<>();

    @Override
    public void init() throws ServletException {
        page.put(1, "/DispatcherServlet");
        page.put(2, "/DriverServlet");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String save = req.getParameter("save") != null ? req.getParameter("save") : "";

        User checkUser = new User();
        checkUser.setLogin(login);
        checkUser.setPassword(password);
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        UserDAO autoDao = abstractDAO.getUserDao();
        log.debug("USER searching");
        User user = (User) autoDao.get(checkUser);
        if (user != null) {
            log.debug("USER found");
            req.getSession(true).setAttribute("user", user);
            log.debug("USER ready");
            if (save.equals("on")) {
                log.debug("USER remembered");
                req.getSession().setAttribute("save", true);
            }
            resp.sendRedirect(req.getContextPath() + page.get(user.getIdRole()));
        } else {
            log.debug("USER not found");
            resp.sendRedirect(req.getContextPath() + Path.INDEX_PAGE + "?fail=true");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String logout = req.getParameter("logout");
        if (logout.equals("true")) {
            HttpSession session = req.getSession();
            session.invalidate();
            log.info("USER session ended");
            resp.setHeader("Cache-Control", "no-cache");
            resp.setHeader("Cache-Control", "no-store");
            resp.setDateHeader("Expires", 0);
            resp.sendRedirect(req.getContextPath() + Path.INDEX_PAGE);
            log.info("USER out");
        }
    }
}
