package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.service.statement.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Process statement command
 */
@WebServlet("/Statement")
public class StatementServlet extends GenericServlet<Class<StatementServlet>> {

    @Override
    public void init() throws ServletException {
        commandMapPost.put("addStatement", new AddStatement());
        commandMapPost.put("acceptStatement", new AddAcceptStatement());
        commandMapPost.put("finish", new DoneStatement());

        commandMapGet.put("cancel", new CancelStatement());
        commandMapGet.put("cancelStatement", new DropStatement());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
        if (commandMapPost.containsKey(command)) {
            log.debug("STATEMENT#COMMAND:" + command + " found. Executing...");
            commandMapPost.get(command).execute(req, resp);
            log.debug("STATEMENT#COMMAND:" + command + " executed");
        } else {
            log.debug("STATEMENT#COMMAND:" + command + " not found");
            resp.sendRedirect(req.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
        if (commandMapGet.containsKey(command)) {
            log.debug("STATEMENT#COMMAND:" + command + " found. Executing...");
            commandMapGet.get(command).execute(req, resp);
            log.debug("STATEMENT#COMMAND:" + command + " executed");
        } else {
            log.debug("STATEMENT#COMMAND:" + command + " not found");
            resp.sendRedirect(req.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE);
        }
    }
}
