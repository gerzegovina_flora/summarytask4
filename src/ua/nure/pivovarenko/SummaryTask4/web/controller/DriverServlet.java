package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Main driver controller
 */
@WebServlet("/DriverServlet")
public class DriverServlet extends GenericServlet<Class<DriverServlet>> {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        FlightDAO flightDao = abstractDAO.getFlightDao();
        long idUser = ((User) request.getSession().getAttribute("user")).getId();
        log.debug("DRIVER: Loading FREE Flights");
        List<Flight> flights = flightDao.getAllFreeFlights(idUser);
        log.debug("DRIVER: Loading FREE Flights loaded");
        request.getSession().setAttribute("flights", flights);

        StatementDAO statementDao = abstractDAO.getStatementDao();
        log.debug("DRIVER: Loading ACTIVE Statements");
        List<Statement> activeStatements = statementDao.getAllActiveStatements(idUser);
        log.debug("DRIVER: Loading ACTIVE Statements loaded");
        request.getSession().setAttribute("activeStatements", activeStatements);

        List<Statement> sentStatements = statementDao.getSentStatements(idUser);
        log.debug("DRIVER: Loading SENT Statements loaded");
        request.getSession().setAttribute("sentStatements", sentStatements);

        log.debug("DRIVER: Redirect to the account page");
        response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
    }
}