package ua.nure.pivovarenko.SummaryTask4.web.controller;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.service.Command;

import javax.servlet.http.HttpServlet;
import java.util.HashMap;
import java.util.Map;

public abstract class GenericServlet<T extends Class<?>> extends HttpServlet {

    protected final Map<String, Command> commandMapGet = new HashMap<>();
    protected final Map<String, Command> commandMapPost = new HashMap<>();

    protected final Logger log = Logger.getLogger((T) Class.class);
}
