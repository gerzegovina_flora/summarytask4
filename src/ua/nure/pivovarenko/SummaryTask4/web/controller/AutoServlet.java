package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.service.auto.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Process auto command
 */
@WebServlet("/Auto")
public class AutoServlet extends GenericServlet<Class<AutoServlet>> {

    @Override
    public void init() throws ServletException {
        commandMapPost.put("createAuto", new AddAuto());
        commandMapPost.put("dropAutos", new DropSetOfAutos());
        commandMapPost.put("editAuto", new EditAuto());

        commandMapGet.put("deleteAuto", new DropAuto());
        commandMapGet.put("findCar", new FindAuto());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        if (commandMapGet.containsKey(command)) {
            log.debug("AUTO#COMMAND:" + command + " found. Executing...");
            commandMapGet.get(command).execute(request, response);
            log.debug("AUTO#COMMAND:" + command + " executed");
        } else {
            log.debug("AUTO#COMMAND:" + command + " not found");
            response.sendRedirect(request.getContextPath() + Path.AUTO_PAGE + "?success=false");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        if (commandMapPost.containsKey(command)) {
            log.debug("AUTO#COMMAND:" + command + " found. Executing...");
            commandMapPost.get(command).execute(request, response);
            log.debug("AUTO#COMMAND:" + command + " executed");
        } else {
            log.debug("AUTO#COMMAND:" + command + " not found");
            response.sendRedirect(request.getContextPath() + Path.AUTO_PAGE + "?success=false");
        }
    }

}
