package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.service.Command;
import ua.nure.pivovarenko.SummaryTask4.service.user.AddUser;
import ua.nure.pivovarenko.SummaryTask4.service.user.DropSetOfUsers;
import ua.nure.pivovarenko.SummaryTask4.service.user.DropUser;
import ua.nure.pivovarenko.SummaryTask4.service.user.EditUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Pivovarenko D.
 * Main user controller
 */
@WebServlet("/User")
public class UserServlet extends GenericServlet<Class<UserServlet>> {

    private final Map<String, Command> commandMapGet = new HashMap<>();
    private final Map<String, Command> commandMapPost = new HashMap<>();

    @Override
    public void init() throws ServletException {
        commandMapPost.put("createUser", new AddUser());
        commandMapPost.put("editUser", new EditUser());
        commandMapPost.put("dropUsers", new DropSetOfUsers());

        commandMapGet.put("deleteUser", new DropUser());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
        if (commandMapGet.containsKey(command)) {
            log.debug("USER#COMMAND:" + command + " found. Executing...");
            commandMapGet.get(command).execute(req, resp);
            log.debug("USER#COMMAND:" + command + " executed");
        } else {
            log.debug("STATEMENT#COMMAND:" + command + " not found");
            resp.sendRedirect(req.getContextPath() + Path.USER_PAGE + "?success=false");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String command = req.getParameter("command");
        if (commandMapPost.containsKey(command)) {
            log.debug("USER#COMMAND:" + command + " found. Executing...");
            commandMapPost.get(command).execute(req, resp);
            log.debug("USER#COMMAND:" + command + " executed");
        } else {
            log.debug("STATEMENT#COMMAND:" + command + " not found");
            resp.sendRedirect(req.getContextPath() + Path.USER_PAGE + "?success=false");
        }
    }
}
