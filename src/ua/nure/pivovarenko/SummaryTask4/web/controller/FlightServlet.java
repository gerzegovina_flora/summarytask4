package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.service.flight.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Process flight command
 */
@WebServlet("/Flight")
public class FlightServlet extends GenericServlet<Class<FlightServlet>> {

    @Override
    public void init() throws ServletException {
        commandMapPost.put("createFlight", new AddFlight());
        commandMapPost.put("editFlight", new EditFlight());
        commandMapPost.put("dropFlights", new DropSetOfFlights());

        commandMapGet.put("deleteFlight", new DropFlight());
        commandMapGet.put("sortFlight", new SortFlight());
        commandMapGet.put("infoFlight", new FlightInfo());
        commandMapGet.put("sortPoint", new SortPoint());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        if (commandMapGet.containsKey(command)) {
            log.debug("FLIGHT#COMMAND:" + command + " found. Executing...");
            commandMapGet.get(command).execute(request, response);
            log.debug("FLIGHT#COMMAND:" + command + " executed");
        } else {
            log.debug("FLIGHT#COMMAND:" + command + " not found");
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE + "?success=false");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        if (commandMapPost.containsKey(command)) {
            log.debug("FLIGHT#COMMAND:" + command + " found. Executing...");
            commandMapPost.get(command).execute(request, response);
            log.debug("FLIGHT#COMMAND:" + command + " executed");
        } else {
            log.debug("FLIGHT#COMMAND:" + command + " not found");
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE + "?success=false");
        }
    }
}
