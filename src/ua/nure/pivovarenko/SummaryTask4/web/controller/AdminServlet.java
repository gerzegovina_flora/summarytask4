package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Main admin controller
 */
@WebServlet("/Administrator")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = "admin")
)
public class AdminServlet extends GenericServlet<Class<AdminServlet>> {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        user.setIdRole(0);
        user.setLogin("Admin");
        request.getSession().setAttribute("user", user);
        log.debug("ADMINISTRATOR: Logged");
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        FlightDAO flightDao = abstractDAO.getFlightDao();
        log.debug("ADMINISTRATOR: Flights loading");
        List<Flight> flights = flightDao.getAll();
        log.debug("ADMINISTRATOR: Flights loaded");
        request.getSession().setAttribute("flights", flights);
        log.debug("ADMINISTRATOR: Loading data to the application context");
        ServletContext servletContext = request.getServletContext();
        Initializer.initializeAll(servletContext);
        log.debug("ADMINISTRATOR: Application context loaded");
        log.debug("ADMINISTRATOR: Redirect to the account page");
        response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
    }
}
