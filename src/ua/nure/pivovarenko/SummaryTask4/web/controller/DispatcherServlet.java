package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/DispatcherServlet")
public class DispatcherServlet extends GenericServlet<Class<DispatcherServlet>> {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        FlightDAO flightDao = abstractDAO.getFlightDao();
        log.debug("DISPATCHED: Loading Flights");
        List<Flight> flights = flightDao.getAll();
        log.debug("DISPATCHED: Flights loaded");
        request.getSession().setAttribute("flights", flights);
        log.debug("DISPATCHED: Redirect to the account page");
        response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
    }
}