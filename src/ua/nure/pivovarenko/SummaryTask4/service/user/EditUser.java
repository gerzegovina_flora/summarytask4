package ua.nure.pivovarenko.SummaryTask4.service.user;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.core.util.Letter;
import ua.nure.pivovarenko.SummaryTask4.core.util.MailSender;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to edit user
 */
public class EditUser extends GenericService<Class<DropUser>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id").trim();
        String login = request.getParameter("login").trim();
        String password = request.getParameter("password").trim();
        String part = request.getParameter("role").trim();
        String email = request.getParameter("mail").trim();
        try {
            log.debug("Editing USER(id:" + id + ")");
            log.debug("AUTO parameters: login" + login + ", password" + password + ", part" + part);
            Role role = Role.valueOf(part);
            User user = new User();
            user.setId(Long.parseLong(id));
            user.setLogin(login);
            user.setPassword(password);
            user.setRole(role);
            user.setIdRole(role.getId());
            user.setMail(email);
            UserValidator userValidator = new UserValidator(request);
            if (userValidator.isValid(user)) {
                AbstractDAO abstractDAO = AbstractDAO.getInstance();
                UserDAO userDao = abstractDAO.getUserDao();
                userDao.update(user);
                log.debug("USER (id:" + id + ") edited");
                log.debug("List of USER refreshing");
                ServletContext servletContext = request.getServletContext();
                Initializer.initializeUsers(servletContext);
                log.debug("List of USER refreshed");

                String log = request.getServletContext().getInitParameter("e-login");
                String pass = request.getServletContext().getInitParameter("e-password");

                Letter letter = new Letter();
                letter.setText(Letter.userUpdatedMessage(user));
                letter.setSubject(Letter.SUBJECT);
                letter.setTo(email);
                MailSender.sendMessage(log, pass, letter);

                response.sendRedirect(request.getContextPath() + Path.USER_PAGE);
            } else {
                request.getRequestDispatcher(Path.EDIT_USER_PAGE).forward(request, response);
            }
        } catch (Exception e) {
            log.error("Error while USER(id:" + id + ") edited", e);
            response.sendRedirect(request.getContextPath() + Path.EDIT_USER_PAGE + "?" + Path.SUCCESS_FALSE + "&id=" + id);
        }
    }
}
