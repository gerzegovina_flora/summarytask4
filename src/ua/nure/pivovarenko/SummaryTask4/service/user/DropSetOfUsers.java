package ua.nure.pivovarenko.SummaryTask4.service.user;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.util.Converter;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Pivovarenko D.
 * Class to delete users
 */
public class DropSetOfUsers extends GenericService<Class<DropSetOfUsers>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Deleting USERS");
        String[] autos = request.getParameterValues("checkedUser");
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        UserDAO userDao = abstractDAO.getUserDao();
        try {
            long[] ids = Converter.convertFromStringToInt(autos);
            log.debug("USERS selected: " + Arrays.toString(ids));
            userDao.deleteFew(ids);
            log.debug("USERS deleted");
            log.debug("List of USERS refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeUsers(servletContext);
            log.debug("List of USERS refreshed");
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while USERS deleting", e);
            response.sendRedirect(request.getContextPath() + Path.USER_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
