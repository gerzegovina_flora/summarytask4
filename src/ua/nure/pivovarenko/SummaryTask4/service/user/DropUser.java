package ua.nure.pivovarenko.SummaryTask4.service.user;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to delete user
 */
public class DropUser extends GenericService<Class<DropUser>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        log.debug("Deleting USER(id:" + id + ")");
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        UserDAO userDao = abstractDAO.getUserDao();
        try {
            User user = new User();
            user.setId(Long.parseLong(id));
            userDao.delete(user);
            log.debug("USER(id:" + id + ") removed");
            log.debug("List of USER refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeUsers(servletContext);
            log.debug("List of USER refreshed");
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while USER removed", e);
            response.sendRedirect(request.getContextPath() + Path.USER_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
