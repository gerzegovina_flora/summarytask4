package ua.nure.pivovarenko.SummaryTask4.service.user;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.core.util.Letter;
import ua.nure.pivovarenko.SummaryTask4.core.util.MailSender;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 *         Class to create user
 */
public class AddUser extends GenericService<Class<AddUser>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Adding USER");
        String login = request.getParameter("login").trim();
        String password = request.getParameter("password").trim();
        String part = request.getParameter("role").trim();
        String mail = request.getParameter("mail").trim();

        log.debug("AUTO parameters: login: " + login + ", password: " + password + ", part: " + part);
        Role role = Role.valueOf(part);
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setIdRole(role.getId());
        user.setMail(mail);
        user.setRole(role);
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        UserDAO userDao = abstractDAO.getUserDao();
        log.debug("Check USER's unique login");
        if (userDao.checkUser(user) == null) {
            log.debug("USER login is unique: " + login);
            UserValidator userValidator = new UserValidator(request);
            if (userValidator.isValid(user)) {
                userDao.create(user);
                log.debug("USER has been added");
                log.debug("List of USERS refreshed");
                ServletContext servletContext = request.getServletContext();
                Initializer.initializeUsers(servletContext);
                response.sendRedirect(request.getContextPath() + Path.ADD_USER_PAGE + "?" + Path.SUCCESS_TRUE);

                String log = request.getServletContext().getInitParameter("e-login");
                String pass = request.getServletContext().getInitParameter("e-password");

                Letter letter = new Letter();
                letter.setText(Letter.userCreateMessage(user));
                letter.setSubject(Letter.SUBJECT);
                letter.setTo(mail);
                MailSender.sendMessage(log, pass, letter);
            } else {
                request.getRequestDispatcher(Path.ADD_USER_PAGE).forward(request, response);
            }
        } else {
            log.debug("USER login is exist: " + login);
            request.setAttribute("userExistFail", true);
            request.getRequestDispatcher(Path.ADD_USER_PAGE).forward(request, response);
        }
    }
}
