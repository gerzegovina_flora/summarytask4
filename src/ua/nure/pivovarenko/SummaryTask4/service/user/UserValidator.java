package ua.nure.pivovarenko.SummaryTask4.service.user;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * User's parameter validator
 */
public class UserValidator {

    private static final Logger LOG = Logger.getLogger(UserValidator.class);

    private HttpServletRequest request;

    public UserValidator(HttpServletRequest request) {
        this.request = request;
    }

    public boolean isValid(User user) throws ServletException, IOException {
        boolean success = true;
        if (!Validator.login(user.getLogin())) {
            LOG.error("USER [login: " + user.getLogin() + "] attribute isn't valid");
            success = false;
            request.setAttribute("loginFail", true);
        }
        if (!Validator.mail(user.getMail())) {
            LOG.error("USER [mail: " + user.getMail() + "] attribute isn't valid");
            success = false;
            request.setAttribute("mailFail", true);
        }
        if (!user.getPassword().equals("")) {
            if (!Validator.password(user.getPassword())) {
                LOG.error("USER [password: " + user.getPassword() + "] attribute isn't valid");
                success = false;
                request.setAttribute("passwordFail", true);
            }
        } else {
            LOG.error("USER password has not been changed");
        }
        return success;
    }

}
