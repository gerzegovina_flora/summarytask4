package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to create flight
 */
public class AddFlight extends GenericService<Class<AddFlight>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Adding FLIGHT");
        String from = request.getParameter("from").trim();
        String to = request.getParameter("to").trim();
        String status = request.getParameter("status").trim();
        String description = request.getParameter("description").trim();

        log.debug("FLIGHT parameters: from: " + from + ", to: " + to + ", status: " + status);
        Flight flight = new Flight();
        flight.setFrom(from);
        flight.setTo(to);
        flight.setFlightStatus(FlightStatus.valueOf(status));
        flight.setDescription(description);

        FlightValidator flightValidator = new FlightValidator(request);

        if (flightValidator.isValid(flight)) {
            log.debug("FLIGHT data is valid");

            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            FlightDAO flightDao = abstractDAO.getFlightDao();
            flightDao.create(flight);

            log.debug("FLIGHT has been added");
            log.debug("List of FLIGHTS refreshing");
            List<Flight> flights = flightDao.getAll();
            request.getSession().setAttribute("flights", flights);

            response.sendRedirect(request.getContextPath() + Path.ADD_FLIGHT_PAGE + "?" + Path.SUCCESS_TRUE);
        } else {
            request.getRequestDispatcher(Path.ADD_FLIGHT_PAGE).forward(request, response);
        }
    }
}
