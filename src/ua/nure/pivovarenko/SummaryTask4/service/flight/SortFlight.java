package ua.nure.pivovarenko.SummaryTask4.service.flight;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Order;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Sort;
import ua.nure.pivovarenko.SummaryTask4.core.util.SortCollection;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.service.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 *         Sorts flight
 */
public class SortFlight implements Command {

    private static final Logger LOG = Logger.getLogger(SortFlight.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.debug("Applying SORT");
        String sort = request.getParameter("sort").trim();
        String order = request.getParameter("order").trim();
        List<Flight> flightList = (List<Flight>) request.getSession().getAttribute("flights");
        switch (Sort.valueOf(sort)) {
            case NUMBER:
                if (Order.valueOf(order).ordinal() == 0) {
                    LOG.debug("SORT by FLIGHTS Id(INCREASE)");
                    SortCollection.sortByFlightIdIncrease(flightList);
                } else {
                    LOG.debug("SORT by FLIGHTS Id(DECREASE)");
                    SortCollection.sortByFlightIdDecrease(flightList);
                }
                break;
            case DATE:
                if (Order.valueOf(order).ordinal() == 0) {
                    LOG.debug("SORT by FLIGHTS Date(INCREASE)");
                    SortCollection.setSortByDateCreatedIncrease(flightList);
                } else {
                    LOG.debug("SORT by FLIGHTS Date(DECREASE)");
                    SortCollection.setSortByDateCreatedDecrease(flightList);
                }
                break;
            case STATUS:
                if (Order.valueOf(order).ordinal() == 0) {
                    LOG.debug("SORT by FLIGHTS Status(INCREASE)");
                    SortCollection.setSortByStatusIncrease(flightList);
                } else {
                    LOG.debug("SORT by FLIGHTS Status(DECREASE)");
                    SortCollection.setSortByStatusDecrease(flightList);
                }
                break;
            default:
                break;
        }
        String url = request.getHeader("referer").concat("?sort");
        url = url.replaceAll("sort.*", "sort=" + sort + "&order=" + order);
        response.sendRedirect(url);
    }
}
