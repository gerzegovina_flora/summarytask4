package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to delete flight
 */
public class DropFlight extends GenericService<Class<DropFlight>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        log.debug("Deleting FLIGHT(id:" + id + ")");
        try {
            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            FlightDAO flightDAO = abstractDAO.getFlightDao();
            Flight flight = new Flight();
            flight.setId(Long.parseLong(id));
            flightDAO.delete(flight);
            log.debug("FLIGHT(id:" + id + ") removed");
            List<Flight> flights;
            log.debug("List of FLIGHTS refreshing");
            if (((User) request.getSession().getAttribute("user")).getIdRole() == 2) {
                flights = flightDAO.getAllFreeFlights(((User) request.getSession().getAttribute("user")).getId());
            } else {
                flights = flightDAO.getAll();
            }
            log.debug("List of FLIGHTS refreshed");
            request.getSession().setAttribute("flights", flights);
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeStatements(servletContext);
            Initializer.initializeAcceptedStatements(servletContext);
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while FLIGHT(id:" + id + ") removing", e);
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
