package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.util.Converter;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to delete flights
 */
public class DropSetOfFlights extends GenericService<Class<DropSetOfFlights>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Deleting FLIGHTS");
        String[] flights = request.getParameterValues("checkedFlight");

        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        FlightDAO flightDao = abstractDAO.getFlightDao();
        try {
            long[] ids = Converter.convertFromStringToInt(flights);
            log.debug("FLIGHTS selected: " + Arrays.toString(ids));
            flightDao.deleteFew(ids);
            log.debug("FLIGHTS deleted");
            log.debug("List of FLIGHTS refreshing");
            List<Flight> flightList;
            if (((User) request.getSession().getAttribute("user")).getIdRole() == 2) {
                flightList = flightDao.getAllFreeFlights(((User) request.getSession().getAttribute("user")).getId());
            } else {
                flightList = flightDao.getAll();
            }
            log.debug("List of FLIGHTS refreshed");
            request.getSession().setAttribute("flights", flightList);
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while FLIGHTS deleting", e);
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
