package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to view flight info
 */
public class FlightInfo extends GenericService<Class<FlightInfo>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("FLIGHT information");
        String id = request.getParameter("id");

        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        FlightDAO flightDao = abstractDAO.getFlightDao();
        try {
            log.debug("FLIGHT(id:" + id + ") info is loading");
            Flight flight = new Flight();
            flight.setId(Long.parseLong(id));
            flight = (Flight) flightDao.get(flight);
            log.debug("FLIGHT(id:" + id + ") info is loaded");
            request.setAttribute("id", id);
            request.setAttribute("from", flight.getFrom());
            request.setAttribute("to", flight.getTo());
            request.setAttribute("description", flight.getDescription());
            request.setAttribute("date", flight.getDateCreate());
            request.setAttribute("status", flight.getFlightStatus());
            request.getRequestDispatcher(Path.FLIGHT_INFO_PAGE).forward(request, response);
        } catch (Exception e) {
            log.error("Error while FLIGHT(id:" + id + ") info is loading", e);
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
