package ua.nure.pivovarenko.SummaryTask4.service.flight;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Flight's parameter validator
 */
public class FlightValidator {

    private static final Logger LOG = Logger.getLogger(FlightValidator.class);

    private HttpServletRequest request;

    public FlightValidator(HttpServletRequest request) {
        this.request = request;
    }

    public boolean isValid(Flight flight) throws ServletException, IOException {
        boolean success = true;
        if (flight.getFrom().equals(flight.getTo())) {
            LOG.error("Flight attributes[from,to] are equals");
            success = false;
            request.setAttribute("duplicationFail", true);
        }
        if (!Validator.point(flight.getFrom())) {
            LOG.error("Flight [from: " + flight.getFrom() + "] attribute isn't valid");
            success = false;
            request.setAttribute("fromFail", true);
        }
        if (!Validator.point(flight.getTo())) {
            LOG.error("Flight [to: " + flight.getTo() + "] attribute isn't valid");
            success = false;
            request.setAttribute("toFail", true);
        }
        return success;
    }

}

