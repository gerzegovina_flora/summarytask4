package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to edit flight
 */
public class EditFlight extends GenericService<Class<EditFlight>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Editing FLIGHT");
        String id = request.getParameter("id").trim();
        String from = request.getParameter("from").trim();
        String to = request.getParameter("to").trim();
        String status = request.getParameter("status").trim();
        String description = request.getParameter("description").trim();

        FlightValidator flightValidator = new FlightValidator(request);
        log.debug("FLIGHT(id:" + id + ") parameters: from" + from + ", to" + to + ", status" + status);
        Flight flight = new Flight();
        flight.setId(Long.parseLong(id));
        flight.setFrom(from);
        flight.setTo(to);
        flight.setFlightStatus(FlightStatus.valueOf(status));
        flight.setDescription(description);

        if (flightValidator.isValid(flight)) {
            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            FlightDAO flightDAO = abstractDAO.getFlightDao();
            flightDAO.update(flight);
            log.debug("FLIGHT(id:" + id + ") edited");
            log.debug("List of FLIGHT refreshing");
            List<Flight> flights = flightDAO.getAll();
            request.getSession().setAttribute("flights", flights);
            log.debug("List of FLIGHT refreshed");
            response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
        } else {
            request.getRequestDispatcher(Path.EDIT_FLIGHT_PAGE).forward(request, response);
        }
    }
}
