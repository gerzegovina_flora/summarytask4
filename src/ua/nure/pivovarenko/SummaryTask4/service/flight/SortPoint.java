package ua.nure.pivovarenko.SummaryTask4.service.flight;

import ua.nure.pivovarenko.SummaryTask4.core.util.SortCollection;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.service.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class SortPoint implements Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String from = request.getParameter("from").trim();
        String to = request.getParameter("to").trim();

        Flight flight = new Flight();
        flight.setFrom(from);
        flight.setTo(to);
        List<Flight> flightList = (List<Flight>) request.getSession().getAttribute("flights");

        SortCollection.setSortByFromTo(flightList, flight);
        String url = request.getHeader("referer");
        response.sendRedirect(url);
    }
}
