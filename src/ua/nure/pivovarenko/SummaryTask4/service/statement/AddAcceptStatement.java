package ua.nure.pivovarenko.SummaryTask4.service.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to accepting statement
 */
public class AddAcceptStatement extends GenericService<Class<AddAcceptStatement>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStatement = request.getParameter("idStatement").trim();
        String idAuto = request.getParameter("foundCarId").trim();
        String idFlight = request.getParameter("idFlight").trim();

        if (Validator.isDigit(idStatement) && Validator.isDigit(idAuto) && Validator.isDigit(idFlight)) {
            log.debug("Adding ACCEPT STATEMENT(id:" + idStatement + ")");
            log.debug("Statement data is valid");
            Statement statement = new Statement();
            Flight flight = new Flight();
            Auto auto = new Auto();

            auto.setId(Long.parseLong(idAuto));

            flight.setId(Long.parseLong(idFlight));
            flight.setAuto(auto);

            statement.setId(Long.parseLong(idStatement));
            statement.setFlight(flight);

            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            StatementDAO statementDao = abstractDAO.getStatementDao();
            statementDao.acceptStatement(statement);

            log.debug("Statement has been sent");
            log.debug("Statement data refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAll(servletContext);
            log.debug("Statement data refreshed");
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE);
            return;
        }
        log.error("Statement data isn't valid. Please check data(ID_AUTO: " + idAuto +
                ", ID_FLIGHT:" + idFlight + ", ID_STATEMENT" + idStatement + ")");
        response.sendRedirect(request.getContextPath() + Path.ACCEPT_AUTO_PAGE + "?" + Path.SUCCESS_FALSE);
    }
}
