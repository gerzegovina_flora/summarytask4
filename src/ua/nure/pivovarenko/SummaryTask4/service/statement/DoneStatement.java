package ua.nure.pivovarenko.SummaryTask4.service.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 *         Class to done statement
 */
public class DoneStatement extends GenericService<Class<DoneStatement>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStatement = request.getParameter("idStatement");
        String idFlight = request.getParameter("idFlight");
        String idAuto = request.getParameter("idAuto");
        String flightState = request.getParameter("flightState");
        String autoState = request.getParameter("autoState");

        Statement statement = new Statement();
        Flight flight = new Flight();
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        StatementDAO statementDao = abstractDAO.getStatementDao();
        User user = (User) request.getSession().getAttribute("user");
        log.debug("Ending STATEMENT(id:" + idStatement + ")");
        try {
            Auto auto = new Auto();
            auto.setAutoState(AutoState.valueOf(autoState));
            auto.setId(Long.parseLong(idAuto));

            statement.setId(Long.parseLong(idStatement));
            flight.setId(Long.parseLong(idFlight));
            flight.setFlightStatus(FlightStatus.valueOf(flightState));
            flight.setAuto(auto);

            statement.setFlight(flight);
            statement.setDriver(user);

            statementDao.setDone(statement);
            log.debug("STATEMENT(id:" + idStatement + ") marked as done");
            log.debug("ACTIVE STATEMENTS is refreshing");
            List<Statement> activeStatements = statementDao.getAllActiveStatements(user.getId());
            log.debug("ACTIVE STATEMENTS refreshed");
            request.getSession().setAttribute("activeStatements", activeStatements);
            response.sendRedirect(request.getContextPath() + Path.CURRENT_FLIGHT_PAGE);
        } catch (Exception e) {
            log.debug("ERROR while STATEMENT(id:" + idStatement + ") ending", e);
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.FAIL_TRUE);
        }
    }
}
