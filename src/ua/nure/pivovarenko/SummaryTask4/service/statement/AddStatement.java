package ua.nure.pivovarenko.SummaryTask4.service.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.CharacteristicValidator;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to create statement
 */
public class AddStatement extends GenericService<Class<AddStatement>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("ADDING STATEMENT");
        String weight = request.getParameter("weight").trim();
        String placeCount = request.getParameter("countPlace").trim();
        String weightCapacity = request.getParameter("weightCapacity");
        String idDriver = request.getParameter("idDriver").trim();
        String idFlight = request.getParameter("idFlight").trim();
        String autoType = request.getParameter("auto_type").trim();
        String engineType = request.getParameter("engine").trim();
        String gearType = request.getParameter("gear").trim();

        CharacteristicValidator characteristicValidator = new CharacteristicValidator(request);

        try {
            log.debug("STATEMENT parameters: weight: " + weight + ", placeCount: " + placeCount + ", weightCapacity: " + weightCapacity + ", idDriver: " + idDriver + ", idFlight: " + idFlight + ", autoType: " + autoType + ", engineType: " + engineType + ", gearType: " + gearType);
            User driver = new User();
            long idUser = Long.parseLong(idDriver);
            driver.setId(idUser);

            Flight flight = new Flight();
            flight.setId(Long.parseLong(idFlight));
            Characteristic characteristic = new Characteristic();

            Statement statement = new Statement();
            statement.setDriver(driver);
            statement.setFlight(flight);

            if (characteristicValidator.isValid(weight, placeCount, weightCapacity)) {
                log.debug("STATEMENT data is valid");
                characteristic.setPlaceCount(Integer.parseInt(placeCount));
                characteristic.setWeight(Float.parseFloat(weight));
                characteristic.setAutoType(AutoType.valueOf(autoType));
                characteristic.setWeightCapacity(Float.parseFloat(weightCapacity));
                characteristic.setEngineType(EngineType.valueOf(engineType));
                characteristic.setGearType(GearType.valueOf(gearType));
                statement.setCharacteristic(characteristic);

                AbstractDAO abstractDAO = AbstractDAO.getInstance();
                StatementDAO statementDao = abstractDAO.getStatementDao();
                statementDao.create(statement);
                log.debug("STATEMENT has been added");

                FlightDAO flightDao = abstractDAO.getFlightDao();
                log.debug("List of FREE FLIGHTS refreshing");
                List<Flight> flights = flightDao.getAllFreeFlights(((User) request.getSession().getAttribute("user")).getId());
                request.getSession().setAttribute("flights", flights);
                log.debug("List of FREE FLIGHTS refreshed");
                log.debug("List of STATEMENTS refreshing");
                ServletContext servletContext = request.getServletContext();
                Initializer.initializeStatements(servletContext);
                log.debug("STATEMENT refreshed");

                StatementDAO statementDAO = abstractDAO.getStatementDao();
                List<Statement> sentStatements = statementDAO.getSentStatements(idUser);
                log.debug("DRIVER: Loading SENT Statements");
                request.getSession().setAttribute("sentStatements", sentStatements);
                log.debug("DRIVER: SENT Statements loaded");

                response.sendRedirect(request.getContextPath() + Path.ACCOUNT_PAGE);
            } else {
                request.getRequestDispatcher(Path.ADD_STATEMENT_PAGE).forward(request, response);
            }
        } catch (RuntimeException e) {
            log.error("Error while adding STATEMENT", e);
            response.sendRedirect(request.getContextPath() + Path.ADD_STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE + "&idFlight=" + idFlight);
        }
    }

}
