package ua.nure.pivovarenko.SummaryTask4.service.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 *         Class to cancel statement
 */
public class CancelStatement extends GenericService<Class<CancelStatement>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStatement = request.getParameter("idStatement").trim();
        String idFlight = request.getParameter("idFlight").trim();

        Statement statement = new Statement();
        Flight flight = new Flight();
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        StatementDAO statementDao = abstractDAO.getStatementDao();
        log.debug("Canceling STATEMENT(id:" + idStatement + ")");
        try {
            statement.setId(Long.parseLong(idStatement));
            flight.setId(Long.parseLong(idFlight));

            statement.setFlight(flight);
            statementDao.setCancel(statement);
            log.debug("STATEMENT(id:" + idStatement + ") has been canceled");
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE);
        } catch (Exception e) {
            log.debug("Error while canceling STATEMENT(id:" + idStatement + ")", e);
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE);
        }
    }
}
