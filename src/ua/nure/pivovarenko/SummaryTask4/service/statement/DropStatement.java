package ua.nure.pivovarenko.SummaryTask4.service.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to delete statement
 */
public class DropStatement extends GenericService<Class<DoneStatement>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idStatement = request.getParameter("id");
        String idCharacteristic = request.getParameter("idCharacteristic");
        log.debug("Deleting STATEMENT(id:" + idStatement + ")");
        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        StatementDAO statementDAO = abstractDAO.getStatementDao();
        Statement statement = new Statement();
        Characteristic characteristic = new Characteristic();
        try {
            statement.setId(Long.parseLong(idStatement));
            characteristic.setId(Long.parseLong(idCharacteristic));
            statement.setCharacteristic(characteristic);
            statementDAO.delete(statement);
            log.debug("STATEMENT deleted");
            log.debug("List of ACCEPTED_STATEMENTS refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAcceptedStatements(servletContext);
            log.debug("List of ACCEPTED_STATEMENTS refreshed");
            log.debug("List of STATEMENTS refreshing");
            Initializer.initializeStatements(servletContext);
            log.debug("List of STATEMENTS refreshed");
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while STATEMENT(id:" + idStatement + ") removing", e);
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE);
        }
    }
}
