package ua.nure.pivovarenko.SummaryTask4.service.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.*;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 *         Class to create auto
 */
public class AddAuto extends GenericService<Class<AddAuto>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Adding AUTO");
        String weight = request.getParameter("weight").trim();
        String placeCount = request.getParameter("count_place").trim();
        String carType = request.getParameter("car_type").trim();
        String state = request.getParameter("state").trim();
        String name = request.getParameter("name").trim();
        String engineType = request.getParameter("engine").trim();
        String gearType = request.getParameter("gear").trim();
        String weightCapacity = request.getParameter("weightCapacity").trim();

        log.debug("AUTO parameters: weight: " + weight + ", placeCount: " + placeCount + ", carType: " + carType + ", state: " + state + ", name: " + name + ", engineType: " + engineType + ", gearType: " + gearType + ", weightCapacity: " + weightCapacity);
        Auto auto = new Auto();
        auto.setName(name);
        auto.setAutoState(AutoState.valueOf(state));

        Characteristic characteristic = new Characteristic();
        characteristic.setAutoType(AutoType.valueOf(carType));
        characteristic.setEngineType(EngineType.valueOf(engineType));
        characteristic.setGearType(GearType.valueOf(gearType));

        AutoValidator autoValidator = new AutoValidator(request);

        if (autoValidator.isValid(auto, weight, placeCount, weightCapacity)) {
            log.debug("AUTO data is valid");
            characteristic.setPlaceCount(Integer.parseInt(placeCount));
            characteristic.setWeight(Float.parseFloat(weight.replaceAll(",", ".")));
            characteristic.setWeightCapacity(Float.parseFloat(weightCapacity.replaceAll(",", ".")));
            auto.setCharacteristic(characteristic);

            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            AutoDAO autoDaoImp = abstractDAO.getAutoDao();
            autoDaoImp.create(auto);

            log.debug("AUTO has been added");
            log.debug("List of AUTOS refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAutos(servletContext);
            log.debug("List of AUTOS has been refreshed");
            response.sendRedirect(request.getContextPath() + Path.ADD_AUTO_PAGE + "?" + Path.SUCCESS_TRUE);
        } else {
            request.getRequestDispatcher(Path.ADD_AUTO_PAGE).forward(request, response);
        }
    }

}
