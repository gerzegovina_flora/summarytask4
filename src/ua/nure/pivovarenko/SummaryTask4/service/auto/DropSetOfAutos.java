package ua.nure.pivovarenko.SummaryTask4.service.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.util.Converter;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Pivovarenko D.
 * Class to delete autos
 */
public class DropSetOfAutos extends GenericService<Class<DropSetOfAutos>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Deleting AUTOS");
        String[] autos = request.getParameterValues("checkedAuto");
        try {
            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            AutoDAO autoDao = abstractDAO.getAutoDao();
            long[] ids = Converter.convertFromStringToInt(autos);
            log.debug("AUTOS selected: " + Arrays.toString(ids));
            autoDao.deleteFew(ids);
            log.debug("AUTOS deleted");
            log.debug("List of AUTOS refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAutos(servletContext);
            log.debug("List of AUTOS refreshed");
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while AUTOS deleting", e);
            response.sendRedirect(request.getContextPath() + Path.AUTO_PAGE + "?" + Path.FAIL_TRUE);
        }
    }

}