package ua.nure.pivovarenko.SummaryTask4.service.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Class to delete auto
 */
public class DropAuto extends GenericService<Class<DropAuto>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");

        AbstractDAO abstractDAO = AbstractDAO.getInstance();
        AutoDAO autoDao = abstractDAO.getAutoDao();
        log.debug("Deleting AUTO(id:" + id + ")");
        try {
            Auto auto = new Auto();
            auto.setId(Long.parseLong(id));
            autoDao.delete(auto);
            log.debug("AUTO(id:" + id + ") deleted");
            log.debug("List of AUTOS is refering");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAutos(servletContext);
            log.debug("List of AUTOS is refered");
            String url = request.getHeader("referer");
            response.sendRedirect(url);
        } catch (Exception e) {
            log.error("Error while AUTO(id:" + id + ") deleting", e);
            response.sendRedirect(request.getContextPath() + Path.AUTO_PAGE + "?" + Path.FAIL_TRUE);
        }
    }

}
