package ua.nure.pivovarenko.SummaryTask4.service.auto;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Auto's parameter validator
 */
public class AutoValidator {

    private static final Logger LOG = Logger.getLogger(AutoValidator.class);

    private HttpServletRequest request;

    public AutoValidator(HttpServletRequest request) {
        this.request = request;
    }

    public boolean isValid(Auto auto, String weight, String placeCount, String weightCapacity) throws ServletException, IOException {
        boolean success = true;
        final String notValidAttribute = " attribute isn't valid";
        if (!Validator.isDigit(placeCount)) {
            LOG.error("AUTO placeCount:" + placeCount + notValidAttribute);
            success = false;
            request.setAttribute("placeCountFail", true);
        }
        if (!Validator.isIrrationalNumber(weight)) {
            LOG.error("AUTO weight:" + weight + notValidAttribute);
            success = false;
            request.setAttribute("weightFail", true);
        }
        if (!Validator.isIrrationalNumber(weightCapacity)) {
            LOG.error("AUTO weightCapacity: " + weight + notValidAttribute);
            success = false;
            request.setAttribute("weightCapacity", true);
        }
        if (!Validator.name(auto.getName())) {
            LOG.error("AUTO name:" + auto.getName() + notValidAttribute);
            success = false;
            request.setAttribute("nameFail", true);
        }
        return success;
    }

}
