package ua.nure.pivovarenko.SummaryTask4.service.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.*;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.dao.Initializer;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 *         Class to edit auto
 */
public class EditAuto extends GenericService<Class<EditAuto>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id").trim();
        String weight = request.getParameter("weight").trim();
        String placeCount = request.getParameter("count").trim();
        String name = request.getParameter("name").trim();
        String carType = request.getParameter("type").trim();
        String state = request.getParameter("state").trim();
        String engineType = request.getParameter("engine").trim();
        String gearType = request.getParameter("gear").trim();
        String weightCapacity = request.getParameter("weightCapacity").trim();

        log.debug("Editing AUTO(id:" + id + ")");
        log.debug("AUTO parameters: weight: " + weight + ", placeCount: " + placeCount + ", carType: " + carType + ", state: " + state + ", name: " + name + ", engineType: " + engineType + ", gearType: " + gearType + ", weightCapacity: " + weightCapacity);
        Auto auto = new Auto();
        auto.setId(Long.parseLong(id));
        auto.setName(name);
        auto.setAutoState(AutoState.valueOf(state));

        Characteristic characteristic = new Characteristic();
        characteristic.setAutoType(AutoType.valueOf(carType));
        characteristic.setEngineType(EngineType.valueOf(engineType));
        characteristic.setGearType(GearType.valueOf(gearType));

        AutoValidator autoValidator = new AutoValidator(request);

        if (autoValidator.isValid(auto, weight, placeCount, weightCapacity)) {
            characteristic.setPlaceCount(Integer.parseInt(placeCount));
            characteristic.setWeight(Float.parseFloat(weight.replaceAll(",", ".")));
            characteristic.setWeightCapacity(Float.parseFloat(weightCapacity.replaceAll(",", ".")));

            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            AutoDAO autoDao = abstractDAO.getAutoDao();
            auto.setCharacteristic(characteristic);
            auto.setId(Long.parseLong(id));
            autoDao.update(auto);
            log.debug("AUTO(id:" + id + ") edited");
            log.debug("List of AUTO refreshing");
            ServletContext servletContext = request.getServletContext();
            Initializer.initializeAutos(servletContext);
            log.debug("List of AUTO refreshed");
            response.sendRedirect(request.getContextPath() + Path.AUTO_PAGE);
        } else {
            request.getRequestDispatcher(Path.EDIT_AUTO_PAGE).forward(request, response);
        }
    }
}
