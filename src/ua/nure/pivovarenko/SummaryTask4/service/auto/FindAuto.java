package ua.nure.pivovarenko.SummaryTask4.service.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Path;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;
import ua.nure.pivovarenko.SummaryTask4.dao.AbstractDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDAO;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.service.GenericService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Class to find auto
 */
public class FindAuto extends GenericService<Class<FindAuto>> {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("Searching AUTO");
        String idStatement = request.getParameter("idStatement").trim();
        String idFlight = request.getParameter("idFlight").trim();
        String weight = request.getParameter("weight").trim();
        String weightCapacity = request.getParameter("weightCapacity");
        String placeCount = request.getParameter("placeCount").trim();
        String engineType = request.getParameter("engine").trim();
        String gearType = request.getParameter("gear").trim();
        String autoType = request.getParameter("autoType").trim();

        if (Validator.isDigit(idStatement) && Validator.isDigit(idFlight)
                && Validator.isIrrationalNumber(weight) && Validator.isDigit(placeCount)) {
            log.debug("Criteria are valid");
            Characteristic characteristic = new Characteristic();
            characteristic.setPlaceCount(Integer.parseInt(placeCount));
            characteristic.setWeight(Float.parseFloat(weight));
            characteristic.setWeightCapacity(Float.parseFloat(weightCapacity));
            characteristic.setEngineType(EngineType.valueOf(engineType));
            characteristic.setGearType(GearType.valueOf(gearType));
            characteristic.setAutoType(AutoType.valueOf(autoType));

            AbstractDAO abstractDAO = AbstractDAO.getInstance();
            AutoDAO autoDao = abstractDAO.getAutoDao();
            log.debug("AUTOS searching...");
            List<Auto> carList = autoDao.getSpecifyAutos(characteristic);
            log.debug("List with found AUTOS loaded. Total autos: " + carList.size());
            request.setAttribute("reqCar", carList);
            request.setAttribute("idStatement", idStatement);
            request.setAttribute("idFlight", idFlight);
            request.getRequestDispatcher(Path.ACCEPT_AUTO_PAGE).forward(request, response);
        } else {
            log.error("Error while AUTOS are searching");
            response.sendRedirect(request.getContextPath() + Path.STATEMENT_PAGE + "?" + Path.SUCCESS_FALSE);
        }
    }
}
