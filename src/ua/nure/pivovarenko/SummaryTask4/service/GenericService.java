package ua.nure.pivovarenko.SummaryTask4.service;


import org.apache.log4j.Logger;

public abstract class GenericService<T extends Class<?>> implements Command {
    protected final Logger log = Logger.getLogger((T)Class.class);
}
