package ua.nure.pivovarenko.SummaryTask4.core.constant;

public enum FlightStatus implements IdentifiableEnum{
    OPEN(0), PROGRESS(1), CLOSED(2), CANCELED(3);

    private int id;

    FlightStatus(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
