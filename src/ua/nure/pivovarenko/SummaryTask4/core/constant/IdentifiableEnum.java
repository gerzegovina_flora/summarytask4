package ua.nure.pivovarenko.SummaryTask4.core.constant;

public interface IdentifiableEnum {

    int getId();

}
