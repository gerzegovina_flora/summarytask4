package ua.nure.pivovarenko.SummaryTask4.core.constant;

public enum GearType implements IdentifiableEnum{
    FRONT_WHEEL(0),REAR_WHEEL(1),FOUR_WHEEL(2);

    private int id;

    GearType(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
