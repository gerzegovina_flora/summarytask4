package ua.nure.pivovarenko.SummaryTask4.core.constant;

public enum EngineType implements IdentifiableEnum{
    DIESEL(0),PETROL(1),GAS(2);

    private int id;

    EngineType(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
