package ua.nure.pivovarenko.SummaryTask4.core.constant;


public enum AutoType implements IdentifiableEnum{
    TRUCK(0), MINIBUS(1), TIPPER(2), TANK_CAR(3);

    private int id;

    AutoType(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
