package ua.nure.pivovarenko.SummaryTask4.core.constant;

public class Path {
    public static final String INDEX_PAGE = "/index.jsp";
    public static final String ACCOUNT_PAGE = "/jsp/page.jsp";
    public static final String STATEMENT_PAGE = "/jsp/pages/statement/statement_page.jsp";
    public static final String CURRENT_FLIGHT_PAGE = "/jsp/pages/flight/current_flights.jsp";
    public static final String AUTO_PAGE = "/jsp/pages/auto/autos_page.jsp";
    public static final String USER_PAGE = "/jsp/pages/user/users_page.jsp";
    public static final String ACCEPT_AUTO_PAGE = "/jsp/pages/auto/accept_auto_page.jsp";
    public static final String FLIGHT_INFO_PAGE = "/jsp/pages/flight/flight_info.jsp";
    public static final String ADD_FLIGHT_PAGE = "/jsp/pages/flight/add_flight_page.jsp";
    public static final String ADD_USER_PAGE = "/jsp/pages/user/add_user_page.jsp";
    public static final String EDIT_AUTO_PAGE = "/jsp/pages/auto/edit_auto_page.jsp";
    public static final String EDIT_FLIGHT_PAGE = "/jsp/pages/flight/edit_flight_page.jsp";
    public static final String ADD_AUTO_PAGE = "/jsp/pages/auto/add_auto_page.jsp";
    public static final String EDIT_USER_PAGE = "/jsp/pages/user/edit_user.jsp";
    public static final String ADD_STATEMENT_PAGE = "/jsp/pages/statement/add_statement_page.jsp";
    public static final String FLIGHT_SET_DONE = "/jsp/pages/flight/done_flight.jsp";
    public static final String ACCEPTED_STATEMENT_PAGE = "/jsp/pages/statement/accepted_statements_page.jsp";
    public static final String SENT_STATEMENT_PAGE = "/jsp/pages/statement/sent_statements_page.jsp";

    public static final String ERROR_PAGE = "/error.html";

    public static final String FAIL_TRUE = "fail=true";
    public static final String SUCCESS_FALSE = "success=false";
    public static final String SUCCESS_TRUE = "success=true";
    public static final String AUTH_FALSE = "auth=false";
}
