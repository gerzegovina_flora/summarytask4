package ua.nure.pivovarenko.SummaryTask4.core.constant;

public enum AutoState implements IdentifiableEnum {

    FREE(0), BRAKE(1), BUSY(2);

    private int id;

    AutoState(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
