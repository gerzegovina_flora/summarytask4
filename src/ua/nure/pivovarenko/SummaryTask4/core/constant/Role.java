package ua.nure.pivovarenko.SummaryTask4.core.constant;

public enum Role implements IdentifiableEnum{
    ADMIN(0), DISPATCHER(1), DRIVER(2);

    private int id;

    Role(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
