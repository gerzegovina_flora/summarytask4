package ua.nure.pivovarenko.SummaryTask4.core.util;

import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Sorts flight
 */
public class SortCollection {

    private static Flight exmplflight;

    private static final Comparator SORT_BY_FLIGHT_ID_INCREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return Long.compare(o1.getId(),o2.getId());
        }
    };

    private static final Comparator SORT_BY_DATE_CREATED_INCREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return o1.getDateCreate().compareTo(o2.getDateCreate());
        }
    };

    private static final Comparator SORT_BY_STATUS_INCREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return o1.getFlightStatus().name().compareTo(o2.getFlightStatus().name());
        }
    };

    private static final Comparator SORT_BY_FLIGHT_ID_DECREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return Long.compare(o2.getId(), o1.getId());
        }
    };

    private static final Comparator SORT_BY_DATE_CREATED_DECREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return o2.getDateCreate().compareTo(o1.getDateCreate());
        }
    };

    private static final Comparator SORT_BY_STATUS_DECREASE = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return o2.getFlightStatus().name().compareTo(o1.getFlightStatus().name());
        }
    };

    private static final Comparator SORT_BY_FROM_TO = new Comparator<Flight>() {
        @Override
        public int compare(Flight o1, Flight o2) {
            return o1.getFrom().compareTo(exmplflight.getFrom()) - o2.getTo().compareTo(exmplflight.getTo());
        }
    };

    public static void sortByFlightIdIncrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_FLIGHT_ID_INCREASE);
    }

    public static void setSortByDateCreatedIncrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_DATE_CREATED_INCREASE);
    }

    public static void setSortByStatusIncrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_STATUS_INCREASE);
    }

    public static void sortByFlightIdDecrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_FLIGHT_ID_DECREASE);
    }

    public static void setSortByDateCreatedDecrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_DATE_CREATED_DECREASE);
    }

    public static void setSortByFromTo(List<Flight> flights, Flight flight) {
        exmplflight = flight;
        Collections.sort(flights, SORT_BY_FROM_TO);
    }

    public static void setSortByStatusDecrease(List<Flight> flights) {
        Collections.sort(flights, SORT_BY_STATUS_DECREASE);
    }

}
