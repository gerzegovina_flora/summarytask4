package ua.nure.pivovarenko.SummaryTask4.core.util;

/**
 * @author Pivovarenko D.
 * Converts values
 */
public class Converter {

    public static long[] convertFromStringToInt(String[] strings) {
        long[] ids = new long[strings.length];
        for (int i = 0; i < strings.length; i++) {
            ids[i] = Long.parseLong(strings[i]);
        }
        return ids;
    }

}
