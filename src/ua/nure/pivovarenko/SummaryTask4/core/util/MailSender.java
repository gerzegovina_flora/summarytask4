package ua.nure.pivovarenko.SummaryTask4.core.util;


import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author Pivovarenko D.
 * Sends mail
 */
public class MailSender {

    private static final Logger LOG = Logger.getLogger(MailSender.class);

    public static void sendMessage(final String login, final String password, Letter mess) {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(login, password);
                    }
                });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("dimafun1@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mess.getTo()));
            message.setSubject(mess.getSubject());
            message.setText(mess.getText());
            Transport.send(message);
        } catch (MessagingException e) {
            LOG.error("Messaging error", e);
        }
    }

}
