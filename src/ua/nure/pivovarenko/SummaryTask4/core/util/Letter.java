package ua.nure.pivovarenko.SummaryTask4.core.util;


import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

public class Letter {

    public static final String SUBJECT = "SummaryTask4 Team";

    private String to;
    private String subject;
    private String text;

    public Letter() {
    }

    public Letter(String to) {
        this.to = to;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static String userUpdatedMessage(User user) {
        return "Your data have been changed:\n" +
                "Login: " + user.getLogin() + "\n" +
                "Role: " + user.getRole() + "\n" +
                "Password: " + (!user.getPassword().equals("") ? user.getPassword() : "Not changed");
    }

    public static String userCreateMessage(User user) {
        return "You have been registered:\n" +
                "Login: " + user.getLogin() + "\n" +
                "Role: " + user.getRole() + "\n" +
                "Password: " + user.getPassword();
    }

}
