package ua.nure.pivovarenko.SummaryTask4.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Pivovarenko D.
 * Validate input values
 */
public class Validator {

    public static boolean password(String password) {
        Pattern pattern = Pattern.compile("[0-9a-zA-Z]{6,}");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean name(String name) {
        Pattern pattern = Pattern.compile("^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \\-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public static boolean point(String point) {
        Pattern pattern = Pattern.compile("^(?![ЪъЬьЫы-])[A-ZА-Я]+[' \\-A-Za-zА-ЯЪҐЇІЁа-яїґіъё]{1,35}[^!-@_]$", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(point);
        return matcher.matches();
    }

    public static boolean mail(String point) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(point);
        return matcher.matches();
    }

    public static boolean login(String login) {
        Pattern pattern = Pattern.compile("^(?![!-@])(?!.*[_.]{2})[A-Za-z._[0-9]]{5,100}[^!-/:-@]$");
        Matcher matcher = pattern.matcher(login);
        return matcher.matches();
    }

    public static boolean isDigit(String digit) {
        Pattern pattern = Pattern.compile("^[1-9][0-9]{0,6}");
        Matcher matcher = pattern.matcher(digit);
        return matcher.matches();
    }

    public static boolean isIrrationalNumber(String digit) {
        Pattern pattern = Pattern.compile("^[0-9]+(\\.|,)?[0-9]{1,6}");
        Matcher matcher = pattern.matcher(digit);
        return matcher.matches();
    }
}
