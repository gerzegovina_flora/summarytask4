package ua.nure.pivovarenko.SummaryTask4.persistence;

public interface Identifiable {

    void setId(long id);

    long getId();

}
