package ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain;

import ua.nure.pivovarenko.SummaryTask4.persistence.PersistentEntity;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import java.io.Serializable;

/**
 * @author Pivovarenko D.
 * Statement entity
 */
public class Statement extends PersistentEntity implements Serializable {

    private User driver;
    private Flight flight;
    private Characteristic characteristic;

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User user) {
        this.driver = user;
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(Characteristic characteristic) {
        this.characteristic = characteristic;
    }

    @Override
    public void setId(long id) {
        super.id = id;
    }

    @Override
    public long getId() {
        return super.id;
    }
}
