package ua.nure.pivovarenko.SummaryTask4.persistence.statement;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.dao.DbUtil;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pivovarenko D.
 *         Getting specific info about statements FROM DB
 */
public class StatementDaoImpl extends StatementDAO<Class<StatementDaoImpl>> {

    private static final String CREATE_STATEMENT = "INSERT INTO \"Statement\" (id_characteristic, id_driver, id_flight) VALUES(?,?,?)";
    private static final String CREATE_CHARACTERISTIC = "INSERT INTO \"Characteristic\" (weight, count_place,id_auto_type, weight_capacity, id_engine, id_gear) VALUES(?,?,?,?,?,?)";
    private static final String GET_ALL_STATEMENTS = "SELECT * FROM \"Statement\"\n" +
            "LEFT outer join \"Characteristic\" ON \"Statement\".id_characteristic = \"Characteristic\".id\n" +
            "LEFT outer join \"User\" ON \"User\".id = \"Statement\".id_driver,\n" +
            "\"Flight\", \"AutoType\", \"GearType\", \"EngineType\"\n" +
            "WHERE \"Statement\".accepted IS null OR \"Statement\".accepted = false AND \"AutoType\".\"id\" = \"Characteristic\".id_auto_type\n" +
            "AND \"GearType\".\"id\" = \"Characteristic\".id_gear AND \"EngineType\".\"id\" = \"Characteristic\".id_engine\n" +
            "AND \"Flight\".id = \"Statement\".id_flight";
    private static final String ACCEPT_STATEMENT = "UPDATE \"Statement\" SET accepted = true WHERE id = ?";
    private static final String DELETE_STATEMENT = "DELETE FROM \"Statement\" WHERE id = ?";
    private static final String ACTIVE_STATEMENT = "SELECT \"Flight\".id AS \"flight_id\" ,\"Auto\".\"name\", \"Flight\".\"from\", \"Flight\".\"to\", \"Flight\".id_auto, \"Statement\".\"id\"\n" +
            "FROM \"Statement\"\n" +
            "LEFT OUTER join \"Flight\" ON \"Flight\".\"id\" = \"Statement\".id_flight, \"Auto\"\n" +
            "WHERE \"Statement\".accepted = true AND \"Statement\".id_driver = ? AND \"Auto\".\"id\" = \"Flight\".id_auto";
    private static final String DELETE_UNUSED_STATEMENTS = "DELETE FROM \"Statement\" WHERE \"Statement\".id_flight = ? AND \"Statement\".accepted = false";
    private static final String FINISH = "UPDATE \"Flight\" set id_status = 2 WHERE \"Flight\".\"id\" = ?;" +
            "DELETE FROM \"Statement\" WHERE id = ?";
    private static final String CANCEL = "UPDATE \"Flight\" set id_status = 3 WHERE \"Flight\".\"id\" = ?;" +
            "DELETE FROM \"Statement\" WHERE id = ?";
    private static final String GET_ALL_ACCEPTED_STATEMENTS = "SELECT * FROM \"Statement\" \n" +
            "LEFT OUTER JOIN \"Characteristic\" ON \"Statement\".id_characteristic = \"Characteristic\".id\n" +
            "LEFT OUTER JOIN \"User\" ON \"User\".id = \"Statement\".id_driver,\n" +
            "\"Flight\", \"AutoType\", \"GearType\", \"EngineType\"\n" +
            "WHERE \"Statement\".accepted IS null OR \"Statement\".accepted = true\n" +
            "AND \"Characteristic\".id_auto_type = \"AutoType\".\"id\" AND \"GearType\".\"id\" = \"Characteristic\".id_gear\n" +
            "AND \"EngineType\".\"id\" = \"Characteristic\".id_engine AND \"Flight\".id = \"Statement\".id_flight";
    private static final String DELETE_REQUIREMENT = "DELETE FROM \"Characteristic\" WHERE id = ?";

    public StatementDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public List<Statement> getAllAcceptedStatements() {
        List<Statement> statementList = null;
        Connection connection = null;
        java.sql.Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_ACCEPTED_STATEMENTS);
            statementList = new ArrayList<>();
            while (resultSet.next()) {
                Statement stmt = new Statement();
                Characteristic characteristic = new Characteristic();
                User user = new User();
                Flight flight = new Flight();

                stmt.setId(resultSet.getLong("id"));

                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getInt("id_driver"));

                characteristic.setId(resultSet.getInt("id_characteristic"));
                characteristic.setWeight(resultSet.getFloat("weight"));
                characteristic.setWeightCapacity(resultSet.getFloat("weight_capacity"));
                characteristic.setPlaceCount(resultSet.getInt("count_place"));
                characteristic.setGearType(GearType.valueOf(resultSet.getString("gear_type")));
                characteristic.setEngineType(EngineType.valueOf(resultSet.getString("engine_type")));
                characteristic.setAutoType(AutoType.valueOf(resultSet.getString("type")));

                flight.setId(resultSet.getInt("id_flight"));
                flight.setFrom(resultSet.getString("FROM"));
                flight.setTo(resultSet.getString("to"));

                stmt.setCharacteristic(characteristic);
                stmt.setDriver(user);
                stmt.setFlight(flight);
                statementList.add(stmt);
            }
        } catch (SQLException e) {
            log.error("Error while getting ACCEPTED STATEMENTS", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(statement);
            DbUtil.close(resultSet);
        }
        return statementList;
    }

    public List<Statement> getAllActiveStatements(long id) {
        List<Statement> statementList = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(ACTIVE_STATEMENT);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            statementList = new ArrayList<>();
            while (resultSet.next()) {
                Statement stmt = new Statement();
                Flight flight = new Flight();
                Auto auto = new Auto();

                auto.setName(resultSet.getString("name"));
                auto.setId(resultSet.getLong("id_auto"));

                flight.setId(resultSet.getLong("flight_id"));
                flight.setFrom(resultSet.getString("FROM"));
                flight.setTo(resultSet.getString("to"));
                flight.setAuto(auto);

                stmt.setId(resultSet.getLong("id"));
                stmt.setFlight(flight);

                statementList.add(stmt);
            }
        } catch (SQLException e) {
            log.error("Error while getting ACTIVE STATEMENTS", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(resultSet);
        }
        return statementList;
    }

    public void acceptStatement(Statement statement) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        PreparedStatement preparedStatement3 = null;
        PreparedStatement preparedStatement4 = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(ACCEPT_STATEMENT);
            preparedStatement1.setLong(1, statement.getId());
            preparedStatement1.executeUpdate();
            preparedStatement2 = connection.prepareStatement("UPDATE \"Flight\" SET id_auto = ?, id_status = 1 WHERE id = ?");
            preparedStatement2.setLong(1, statement.getFlight().getAuto().getId());
            preparedStatement2.setLong(2, statement.getFlight().getId());
            preparedStatement2.executeUpdate();
            preparedStatement3 = connection.prepareStatement("UPDATE \"Auto\" SET id_auto_state = 2 WHERE id = ?");
            preparedStatement3.setLong(1, statement.getFlight().getAuto().getId());
            preparedStatement3.executeUpdate();
            preparedStatement4 = connection.prepareStatement(DELETE_UNUSED_STATEMENTS);
            preparedStatement4.setLong(1, statement.getFlight().getId());
            preparedStatement4.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while accepting STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
            DbUtil.close(preparedStatement3);
            DbUtil.close(preparedStatement4);
        }
    }

    public void setDone(Statement statement) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        PreparedStatement preparedStatement3 = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(FINISH);
            preparedStatement1.setLong(1, statement.getFlight().getId());
            preparedStatement1.setLong(2, statement.getId());
            preparedStatement1.executeUpdate();

            preparedStatement2 = connection.prepareStatement("UPDATE \"Auto\" SET id_auto_state = ? WHERE id = ?");
            preparedStatement2.setLong(1, statement.getFlight().getAuto().getAutoState().getId());
            preparedStatement2.setLong(2, statement.getFlight().getAuto().getId());
            preparedStatement2.executeUpdate();

            preparedStatement3 = connection.prepareStatement("UPDATE \"Flight\" SET id_status = ? WHERE id = ?");
            preparedStatement3.setLong(1, statement.getFlight().getFlightStatus().getId());
            preparedStatement3.setLong(2, statement.getFlight().getId());
            preparedStatement3.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while finish STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
            DbUtil.close(preparedStatement3);
        }
    }

    public void setCancel(Statement statement) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(CANCEL);
            preparedStatement.setLong(1, statement.getFlight().getId());
            preparedStatement.setLong(2, statement.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while canceling STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    public List<Statement> getSentStatements(long id) {
        StringBuilder getAllUserStatements = new StringBuilder(GET_ALL_STATEMENTS);
        getAllUserStatements.append(" AND \"User\".\"id\" = ?");
        List<Statement> statementList = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(getAllUserStatements.toString());
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            statementList = new ArrayList<>();
            while (resultSet.next()) {
                Statement stmt = new Statement();
                Characteristic characteristic = new Characteristic();
                User user = new User();
                Flight flight = new Flight();

                stmt.setId(resultSet.getLong("id"));

                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getLong("id_driver"));

                characteristic.setId(resultSet.getLong("id_characteristic"));
                characteristic.setWeight(resultSet.getFloat("weight"));
                characteristic.setWeightCapacity(resultSet.getFloat("weight_capacity"));
                characteristic.setPlaceCount(resultSet.getInt("count_place"));
                characteristic.setGearType(GearType.valueOf(resultSet.getString("gear_type")));
                characteristic.setEngineType(EngineType.valueOf(resultSet.getString("engine_type")));
                characteristic.setAutoType(AutoType.valueOf(resultSet.getString("type")));

                flight.setId(resultSet.getLong("id_flight"));
                flight.setFrom(resultSet.getString("FROM"));
                flight.setTo(resultSet.getString("to"));

                stmt.setCharacteristic(characteristic);
                stmt.setDriver(user);
                stmt.setFlight(flight);
                statementList.add(stmt);
            }
        } catch (SQLException e) {
            log.error("Error while canceling STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
        return statementList;
    }

    @Override
    public List<Statement> getAll() {
        List<Statement> statementList = null;
        Connection connection = null;
        java.sql.Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_STATEMENTS);
            statementList = new ArrayList<>();
            while (resultSet.next()) {
                Statement stmt = new Statement();
                Characteristic characteristic = new Characteristic();
                User user = new User();
                Flight flight = new Flight();

                stmt.setId(resultSet.getInt("id"));

                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getInt("id_driver"));

                characteristic.setId(resultSet.getLong("id_characteristic"));
                characteristic.setWeight(resultSet.getFloat("weight"));
                characteristic.setWeightCapacity(resultSet.getFloat("weight_capacity"));
                characteristic.setPlaceCount(resultSet.getInt("count_place"));
                characteristic.setGearType(GearType.valueOf(resultSet.getString("gear_type")));
                characteristic.setEngineType(EngineType.valueOf(resultSet.getString("engine_type")));
                characteristic.setAutoType(AutoType.valueOf(resultSet.getString("type")));

                flight.setId(resultSet.getLong("id_flight"));
                flight.setFrom(resultSet.getString("FROM"));
                flight.setTo(resultSet.getString("to"));

                stmt.setCharacteristic(characteristic);
                stmt.setDriver(user);
                stmt.setFlight(flight);
                statementList.add(stmt);
            }
        } catch (SQLException e) {
            log.error("Error while getting STATEMENTS", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(statement);
            DbUtil.close(resultSet);
        }
        return statementList;
    }

    @Override
    public Statement get(Statement statement) {
        return null;
    }

    @Override
    public void delete(Statement statement) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(DELETE_STATEMENT);
            preparedStatement1.setLong(1, statement.getId());
            preparedStatement1.executeUpdate();
            preparedStatement2 = connection.prepareStatement(DELETE_REQUIREMENT);
            preparedStatement2.setLong(1, statement.getCharacteristic().getId());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while deleting STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
        }
    }

    @Override
    public void deleteFew(long[] ids) {
    }

    @Override
    public void update(Statement statement) {
    }

    @Override
    public void create(Statement statement) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(CREATE_CHARACTERISTIC, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement1.setFloat(1, statement.getCharacteristic().getWeight());
            preparedStatement1.setInt(2, statement.getCharacteristic().getPlaceCount());
            preparedStatement1.setInt(3, statement.getCharacteristic().getAutoType().getId());
            preparedStatement1.setFloat(4, statement.getCharacteristic().getWeightCapacity());
            preparedStatement1.setInt(5, statement.getCharacteristic().getEngineType().getId());
            preparedStatement1.setInt(6, statement.getCharacteristic().getGearType().getId());
            preparedStatement1.executeUpdate();
            resultSet = preparedStatement1.getGeneratedKeys();
            long lastId = 0;
            if (resultSet.next()) {
                lastId = resultSet.getLong("id");
            }
            preparedStatement2 = connection.prepareStatement(CREATE_STATEMENT);
            preparedStatement2.setLong(1, lastId);
            preparedStatement2.setLong(2, statement.getDriver().getId());
            preparedStatement2.setLong(3, statement.getFlight().getId());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while creating STATEMENT", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
            DbUtil.close(resultSet);
        }
    }
}