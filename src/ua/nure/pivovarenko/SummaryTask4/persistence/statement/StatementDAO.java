package ua.nure.pivovarenko.SummaryTask4.persistence.statement;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.persistence.DaoCrud;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;

import javax.sql.DataSource;
import java.util.List;

public abstract class StatementDAO<T extends Class<?>> implements DaoCrud<Statement>{

    protected final Logger log = Logger.getLogger((T) Class.class);

    protected DataSource dataSource;

    public StatementDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public abstract List<Statement> getAllAcceptedStatements();

    public abstract List<Statement> getAllActiveStatements(long id);

    public abstract void acceptStatement(Statement statement);

    public abstract void setDone(Statement statement);

    public abstract void setCancel(Statement statement);

    public abstract List<Statement> getSentStatements(long id);
}
