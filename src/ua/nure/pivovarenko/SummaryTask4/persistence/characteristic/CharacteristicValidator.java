package ua.nure.pivovarenko.SummaryTask4.persistence.characteristic;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.core.util.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Pivovarenko D.
 * Characteristic's parameter validator
 */
public class CharacteristicValidator {

    private static final Logger LOG = Logger.getLogger(CharacteristicValidator.class);

    private HttpServletRequest request;

    public CharacteristicValidator(HttpServletRequest request) {
        this.request = request;
    }

    public boolean isValid(String weight, String placeCount, String weightCapacity) throws ServletException, IOException {
        boolean success = true;
        if (!Validator.isIrrationalNumber(weight)) {
            LOG.error("Characteristic [weight: " + weight + "] attribute isn't valid");
            success = false;
            request.setAttribute("weightFail", true);
        }
        if (!Validator.isIrrationalNumber(weightCapacity)) {
            LOG.error("Characteristic [weightCapacity: " + weightCapacity + "] attribute isn't valid");
            success = false;
            request.setAttribute("weightCapacityFail", true);
        }
        if (!Validator.isDigit(placeCount)) {
            LOG.error("Characteristic [placeCount: " + placeCount + "] attribute isn't valid");
            success = false;
            request.setAttribute("placeCountFail", true);
        }
        return success;
    }
}
