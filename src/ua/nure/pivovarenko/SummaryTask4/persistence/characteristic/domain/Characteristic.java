package ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.persistence.PersistentEntity;

import java.io.Serializable;

/**
 * @author Pivovarenko D.
 * Characteristic entity
 */
public class Characteristic extends PersistentEntity implements Serializable {

    private AutoType autoType;
    private EngineType engineType;
    private GearType gearType;
    private float weight;
    private int placeCount;
    private float weightCapacity;

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public GearType getGearType() {
        return gearType;
    }

    public void setGearType(GearType gearType) {
        this.gearType = gearType;
    }

    public float getWeightCapacity() {
        return weightCapacity;
    }

    public void setWeightCapacity(float weightCapacity) {
        this.weightCapacity = weightCapacity;
    }

    public AutoType getAutoType() {
        return autoType;
    }

    public void setAutoType(AutoType autoType) {
        this.autoType = autoType;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(int placeCount) {
        this.placeCount = placeCount;
    }

    @Override
    public void setId(long id) {
        super.id = id;
    }

    @Override
    public long getId() {
        return super.id;
    }
}