package ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.persistence.PersistentEntity;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;

import java.io.Serializable;

/**
 * @author Pivovarenko D.
 * Auto entity
 */
public class Auto extends PersistentEntity implements Serializable {

    private AutoState autoState;
    private Characteristic characteristic;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setId(long id) {
        super.id = id;
    }

    @Override
    public long getId() {
        return super.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AutoState getAutoState() {
        return autoState;
    }

    public void setAutoState(AutoState autoState) {
        this.autoState = autoState;
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(Characteristic characteristic) {
        this.characteristic = characteristic;
    }
}
