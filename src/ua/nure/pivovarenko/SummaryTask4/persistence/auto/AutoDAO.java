package ua.nure.pivovarenko.SummaryTask4.persistence.auto;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.persistence.DaoCrud;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;

import javax.sql.DataSource;
import java.util.List;

public abstract class AutoDAO <T extends Class<?>> implements DaoCrud<Auto> {

    protected final Logger log = Logger.getLogger((T) Class.class);

    protected DataSource dataSource;

    public AutoDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public abstract List<Auto> getSpecifyAutos(Characteristic characteristic);
}
