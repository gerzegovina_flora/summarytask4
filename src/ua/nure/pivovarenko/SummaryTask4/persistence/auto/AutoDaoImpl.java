package ua.nure.pivovarenko.SummaryTask4.persistence.auto;

import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.dao.DbUtil;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pivovarenko D.
 * Getting specific info about autos from DB
 */

public class AutoDaoImpl extends AutoDAO<Class<AutoDaoImpl>> {

    private static final String ADD_CHARACTERISTIC = "INSERT INTO \"Characteristic\"(weight, count_place, id_auto, id_auto_type,weight_capacity, id_engine, id_gear) VALUES (?,?,?,?,?,?,?)";
    private static final String ADD_CAR = "INSERT INTO \"Auto\" (id_auto_state, \"name\") VALUES(?,?)";
    private static final String DELETE_CAR = "DELETE FROM \"Auto\" WHERE id = ?";
    private static final String GET_ALL_AUTO = "SELECT * FROM\t\"Auto\" auto ,\"Characteristic\", \"AutoState\", \"AutoType\", \"EngineType\", \"GearType\"\n" +
            "WHERE\t\"AutoState\".\"id\" = auto.id_auto_state AND \"AutoType\".\"id\" = \"Characteristic\".id_auto_type\n" +
            "AND \"EngineType\".\"id\" = \"Characteristic\".id_engine AND \"GearType\". ID = \"Characteristic\".id_gear\n" +
            "AND \"Characteristic\".id_auto = auto. ID";
    private static final String EDIT_AUTO = "UPDATE \"Auto\" SET id_auto_state = ?, \"name\" = ? WHERE id = ?";
    private static final String EDIT_CHARACTERISTIC = "UPDATE \"Characteristic\" SET weight = ?, count_place = ?, id_auto_type = ?, weight_capacity = ?, id_engine = ?, id_gear = ? WHERE id_auto = ?";
    private static final String GET_SPECIFY_AUTO = "SELECT DISTINCT ON(\"Auto\".name) * FROM \"Auto\" " +
            "LEFT OUTER JOIN \"Characteristic\" on \"Characteristic\".id_auto = \"Auto\".\"id\" " +
            "WHERE weight = ? and count_place = ? and weight_capacity = ? and id_engine = ? " +
            "and id_gear = ? and id_auto_type = ? and id_auto_state = 0";

    public AutoDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public List<Auto> getSpecifyAutos(Characteristic characteristic) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        List<Auto> autos = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(GET_SPECIFY_AUTO);
            preparedStatement.setFloat(1, characteristic.getWeight());
            preparedStatement.setInt(2, characteristic.getPlaceCount());
            preparedStatement.setFloat(3, characteristic.getWeightCapacity());
            preparedStatement.setInt(4, characteristic.getEngineType().getId());
            preparedStatement.setInt(5, characteristic.getAutoType().getId());
            preparedStatement.setInt(6, characteristic.getGearType().getId());
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Auto auto = new Auto();
                auto.setId(rs.getInt("id"));
                auto.setName(rs.getString("name"));
                autos.add(auto);
            }
        } catch (SQLException e) {
            log.error("Error while finding specific auto", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(rs);
        }
        return autos;
    }

    @Override
    public void create(Auto auto) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        ResultSet rs = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(ADD_CAR, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement1.setInt(1, auto.getAutoState().getId());
            preparedStatement1.setString(2, auto.getName());
            preparedStatement1.executeUpdate();
            rs = preparedStatement1.getGeneratedKeys();
            int lastId = 0;
            if (rs.next()) {
                lastId = rs.getInt(1);
            }
            preparedStatement2 = connection.prepareStatement(ADD_CHARACTERISTIC);
            preparedStatement2.setFloat(1, auto.getCharacteristic().getWeight());
            preparedStatement2.setInt(2, auto.getCharacteristic().getPlaceCount());
            preparedStatement2.setInt(3, lastId);
            preparedStatement2.setInt(4, auto.getCharacteristic().getAutoType().getId());
            preparedStatement2.setFloat(5, auto.getCharacteristic().getWeightCapacity());
            preparedStatement2.setInt(6, auto.getCharacteristic().getEngineType().getId());
            preparedStatement2.setInt(7, auto.getCharacteristic().getGearType().getId());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while add auto", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
            DbUtil.close(rs);
        }
    }

    @Override
    public List<Auto> getAll() {
        List<Auto> autoList = new ArrayList<>();
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_AUTO);
            while (resultSet.next()) {
                Auto auto = new Auto();
                auto.setName(resultSet.getString("name"));
                auto.setAutoState(AutoState.valueOf(resultSet.getString("state_name")));
                auto.setId(resultSet.getLong("id"));

                Characteristic characteristic = new Characteristic();
                characteristic.setAutoType(AutoType.valueOf(resultSet.getString("type")));
                characteristic.setWeight(resultSet.getFloat("weight"));
                characteristic.setPlaceCount(resultSet.getInt("count_place"));
                characteristic.setGearType(GearType.valueOf(resultSet.getString("gear_type")));
                characteristic.setWeightCapacity(resultSet.getFloat("weight_capacity"));
                characteristic.setEngineType(EngineType.valueOf(resultSet.getString("engine_type")));

                auto.setCharacteristic(characteristic);
                autoList.add(auto);
            }
        } catch (SQLException e) {
            log.error("Error while getting autos", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(statement);
            DbUtil.close(resultSet);
        }
        return autoList;
    }

    @Override
    public Auto get(Auto auto) {
        return null;
    }

    @Override
    public void delete(Auto auto) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(DELETE_CAR);
            preparedStatement.setLong(1, auto.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while deleting auto", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void deleteFew(long[] ids) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(DELETE_CAR);
            for (long id : ids) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while deleting autos", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void update(Auto auto) {
        Connection connection = null;
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(EDIT_AUTO);
            preparedStatement1.setInt(1, auto.getAutoState().getId());
            preparedStatement1.setString(2, auto.getName());
            preparedStatement1.setLong(3, auto.getId());
            preparedStatement1.executeUpdate();
            preparedStatement2 = connection.prepareStatement(EDIT_CHARACTERISTIC);
            preparedStatement2.setFloat(1, auto.getCharacteristic().getWeight());
            preparedStatement2.setInt(2, auto.getCharacteristic().getPlaceCount());
            preparedStatement2.setInt(3, auto.getCharacteristic().getAutoType().getId());
            preparedStatement2.setFloat(4, auto.getCharacteristic().getWeightCapacity());
            preparedStatement2.setInt(5, auto.getCharacteristic().getEngineType().getId());
            preparedStatement2.setInt(6, auto.getCharacteristic().getGearType().getId());
            preparedStatement2.setLong(7, auto.getId());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while updating auto", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement1);
            DbUtil.close(preparedStatement2);
        }
    }
}
