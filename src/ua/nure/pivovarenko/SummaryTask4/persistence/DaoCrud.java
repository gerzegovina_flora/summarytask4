package ua.nure.pivovarenko.SummaryTask4.persistence;

import java.util.List;

public interface DaoCrud<T extends PersistentEntity> {
    List<T> getAll();
    T get(T t);
    void delete(T t);
    void deleteFew(long[] ids);
    void update(T t);
    void create(T t);
}
