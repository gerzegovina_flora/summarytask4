package ua.nure.pivovarenko.SummaryTask4.persistence.flight;

import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.dao.DbUtil;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pivovarenko D.
 *         Getting specific info flights autos from DB
 */
public class FlightDaoImpl extends FlightDAO<Class<FlightDaoImpl>> {

    private static final String CREATE_FLIGHT = "INSERT INTO \"Flight\" (\"from\", \"to\", id_status, date_create, description) VALUES (?,?,?, CURRENT_DATE, ?)";
    private static final String GET_ALL_FLIGHTS = "SELECT * FROM\t\"Flight\" \n" +
            "            LEFT OUTER JOIN \"Statement\" on \"Statement\".id_flight = \"Flight\".\"id\", \"FlightStatus\"\n" +
            "where \"FlightStatus\".\"id\" = \"Flight\".id_status";
    private static final String EDIT_FLIGHT = "UPDATE \"Flight\" SET \"from\" = ? , \"to\" = ?, id_status = ?, description = ? WHERE id = ?";
    private static final String DELETE_FLIGHT = "DELETE FROM \"Flight\" WHERE id = ?";
    private static final String GET_ALL_FREE_FLIGHTS = "SELECT DISTINCT on (\"Flight\".\"id\")* FROM\t\"Flight\" \n" +
            "      left OUTER JOIN \"Statement\" on \"Statement\".id_flight = \"Flight\".\"id\"\n" +
            "\t\t\tLEFT JOIN \"FlightStatus\" on \"FlightStatus\".\"id\" = \"Flight\".id_status\n" +
            "\t\t\twHERE \"Flight\".id_status = 0 and \"Statement\".id_driver != ? or \"Statement\".id_driver is \n" +
            "\t\t\tnull and \"Flight\".\"id_status\" = 0";
    private static final String GET_FLIGHT = "SELECT * FROM\t\"Flight\" LEFT OUTER JOIN \"Statement\" on \"Statement\".id_flight = \"Flight\".\"id\", \"FlightStatus\"\n" +
            "    where \"FlightStatus\".\"id\" = \"Flight\".id_status and \"Flight\".\"id\" = ?";

    public FlightDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public List<Flight> getAllFreeFlights(long id) {
        List<Flight> flights = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(GET_ALL_FREE_FLIGHTS);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            flights = new ArrayList<>();
            while (resultSet.next()) {
                Flight flight = new Flight();
                flight.setId(resultSet.getLong("id"));
                flight.setFrom(resultSet.getString("from"));
                flight.setTo(resultSet.getString("to"));
                flight.setFlightStatus(FlightStatus.valueOf(resultSet.getString("status_name")));
                flight.setDateCreate(resultSet.getDate("date_create"));

                ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement st = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
                st.setId(resultSet.getInt("id"));
                flight.setStatement(st);

                flights.add(flight);
            }
        } catch (SQLException e) {
            log.error("Error while getting FREE FLIGHTS", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(resultSet);
        }
        return flights;
    }

    @Override
    public List<Flight> getAll() {
        List<Flight> flights = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_FLIGHTS);
            flights = new ArrayList<>();
            while (resultSet.next()) {
                Flight flight = new Flight();
                flight.setId(resultSet.getLong("id"));
                flight.setFrom(resultSet.getString("from"));
                flight.setTo(resultSet.getString("to"));
                flight.setFlightStatus(FlightStatus.valueOf(resultSet.getString("status_name")));
                flight.setDateCreate(resultSet.getDate("date_create"));
                flight.setDescription(resultSet.getString("description"));

                ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement st = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
                st.setId(resultSet.getInt("id"));
                flight.setStatement(st);

                flights.add(flight);
            }
        } catch (SQLException e) {
            log.error("Error while getting FLIGHTS");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(statement);
            DbUtil.close(resultSet);
        }
        return flights;
    }

    @Override
    public Flight get(Flight flight) {
        Flight f = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(GET_FLIGHT);
            preparedStatement.setLong(1, flight.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                f = new Flight();

                f.setDescription(resultSet.getString("description"));
                f.setId(resultSet.getLong("id"));
                f.setFrom(resultSet.getString("from"));
                f.setTo(resultSet.getString("to"));
                f.setFlightStatus(FlightStatus.valueOf(resultSet.getString("status_name")));
                f.setDateCreate(resultSet.getDate("date_create"));
            }
        } catch (SQLException e) {
            log.error("Error while getting FLIGHT");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(resultSet);
        }
        return f;
    }

    @Override
    public void delete(Flight flight) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(DELETE_FLIGHT);
            preparedStatement.setLong(1, flight.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while removing FLIGHT");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void deleteFew(long[] ids) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(DELETE_FLIGHT);
            for (long id : ids) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while deleting FLIGHTS");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void update(Flight flight) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(EDIT_FLIGHT);
            preparedStatement.setString(1, flight.getFrom());
            preparedStatement.setString(2, flight.getTo());
            preparedStatement.setInt(3, flight.getFlightStatus().getId());
            preparedStatement.setString(4, flight.getDescription());
            preparedStatement.setLong(5, flight.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while updating FLIGHT");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void create(Flight flight) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_FLIGHT);
            preparedStatement.setString(1, flight.getFrom());
            preparedStatement.setString(2, flight.getTo());
            preparedStatement.setInt(3, flight.getFlightStatus().getId());
            preparedStatement.setString(4, flight.getDescription());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while creating FLIGHT");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }
}
