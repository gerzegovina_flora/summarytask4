package ua.nure.pivovarenko.SummaryTask4.persistence.flight;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.persistence.DaoCrud;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import javax.sql.DataSource;
import java.util.List;

public abstract class FlightDAO<T extends Class<?>> implements DaoCrud<Flight> {

    protected final Logger log = Logger.getLogger((T) Class.class);

    protected DataSource dataSource;

    public FlightDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public abstract List<Flight> getAllFreeFlights(long id);
}
