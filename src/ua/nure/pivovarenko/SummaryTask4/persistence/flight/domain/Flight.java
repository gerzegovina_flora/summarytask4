package ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain;

import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.persistence.PersistentEntity;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Pivovarenko D.
 * Flight entity
 */
public class Flight extends PersistentEntity implements Serializable {

    private String from;
    private String to;
    private Date dateCreate;
    private FlightStatus flightStatus;
    private Statement statement;
    private Auto auto;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDateCreate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(dateCreate);
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = new Date(dateCreate.getTime());
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void setId(long id) {
        super.id = id;
    }

    @Override
    public long getId() {
        return super.id;
    }
}
