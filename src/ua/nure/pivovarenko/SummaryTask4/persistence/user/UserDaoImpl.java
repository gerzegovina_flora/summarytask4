package ua.nure.pivovarenko.SummaryTask4.persistence.user;

import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.dao.DbUtil;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pivovarenko D.
 *         Getting specific info about users from DB
 */
public class UserDaoImpl extends UserDAO<Class<UserDaoImpl>> {

    private static final String ADD_USER = "INSERT INTO \"User\" (id_role, login, password, mail) VALUES(?,?,md5(?),?)";
    private static final String GET_ALL_USERS = "select * from \"User\" u left outer join \"Role\" r on r.id = u.id_role";
    private static final String DROP_USER = "DELETE FROM \"User\" WHERE id = ?";
    private StringBuilder editUser = new StringBuilder("UPDATE \"User\" SET id_role = ?, login = ?, mail = ?");
    private static final String GET_USER = "SELECT * FROM \"User\" WHERE login = ? and PASSWORD = md5(?) ";
    private static final String USER_CONTAINS = "SELECT * FROM \"User\" WHERE login = ?";

    public UserDaoImpl(DataSource dataSource) {
        super(dataSource);
    }

    public User checkUser(User u) {
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(USER_CONTAINS);
            preparedStatement.setString(1, u.getLogin());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            log.error("Error while checking USER");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(resultSet);
        }
        return user;
    }

    @Override
    public User get(User user) {
        User u = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(GET_USER);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                u = new User();
                u.setId(resultSet.getLong("id"));
                u.setIdRole(resultSet.getInt("id_role"));
                u.setLogin(user.getLogin());
                u.setPassword(user.getPassword());
                u.setMail(resultSet.getString("login"));
            }
        } catch (SQLException e) {
            log.error("Error while getting USER");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            DbUtil.close(resultSet);
        }
        return u;
    }

    @Override
    public List<User> getAll() {
        List<User> users = null;
        Connection connection = null;
        java.sql.Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_USERS);
            users = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User();
                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getLong("id"));
                user.setIdRole(resultSet.getInt("id_role"));
                user.setPassword(resultSet.getString("password"));
                user.setRole(Role.valueOf(resultSet.getString("status")));
                user.setMail(resultSet.getString("mail"));
                users.add(user);
            }
        } catch (SQLException e) {
            log.error("Error while getting USERS");
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK");
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(statement);
            DbUtil.close(resultSet);
        }
        return users;
    }

    @Override
    public void delete(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(DROP_USER);
            preparedStatement.setLong(1, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while deleting USER", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void deleteFew(long[] ids) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(DROP_USER);
            for (long id : ids) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            log.error("Error while deleting USERS", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }

    @Override
    public void update(User user) {
        boolean existPassword = !user.getPassword().equals("");
        int index = 1;
        if (existPassword) {
            editUser.append(",password = md5(?)");
        }
        editUser.append(" WHERE id = ?");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(editUser.toString());
            preparedStatement.setInt(index++, user.getIdRole());
            preparedStatement.setString(index++, user.getLogin());
            preparedStatement.setString(index++, user.getMail());
            if (existPassword) {
                preparedStatement.setString(index++, user.getPassword());
            }
            preparedStatement.setLong(index, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while updating USER", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
            editUser = new StringBuilder("UPDATE \"User\" SET id_role = ?, login = ?");
        }
    }

    @Override
    public void create(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement(ADD_USER);
            preparedStatement.setInt(1, user.getIdRole());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getMail());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Error while creating USER", e);
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    log.error("DB internal error: ROLLBACK", e1);
                }
            }
        } finally {
            DbUtil.close(connection);
            DbUtil.close(preparedStatement);
        }
    }
}
