package ua.nure.pivovarenko.SummaryTask4.persistence.user;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.persistence.DaoCrud;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.sql.DataSource;


public abstract class UserDAO<T extends Class<?>> implements DaoCrud<User> {

    protected final Logger log = Logger.getLogger((T) Class.class);

    protected DataSource dataSource;

    public UserDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public abstract User checkUser(User user);

}