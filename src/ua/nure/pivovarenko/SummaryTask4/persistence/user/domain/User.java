package ua.nure.pivovarenko.SummaryTask4.persistence.user.domain;


import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.persistence.PersistentEntity;

import java.io.Serializable;

/**
 * @author Pivovarenko D.
 * User entity
 */

public class User extends PersistentEntity implements Serializable {

    private String login;
    private int idRole;
    private Role role;
    private String password;
    private String mail;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public void setId(long id) {
        super.id = id;
    }

    @Override
    public long getId() {
        return super.id ;
    }
}
