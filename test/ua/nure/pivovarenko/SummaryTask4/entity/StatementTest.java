package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

public class StatementTest {

    private Statement statement;

    @Before
    public void testStatementConstructor(){
        statement = new Statement();
    }

    @Before
    public void testStatementSetters(){
        statement.setCharacteristic(new Characteristic());
        statement.setDriver(new User());
        statement.setFlight(new Flight());
        statement.setId(42);
    }

    @Test
    public void testStatementGetters(){
        Assert.assertEquals(42, statement.getId());
        Assert.assertNotNull(statement.getCharacteristic());
        Assert.assertNotNull(statement.getDriver());
        Assert.assertNotNull(statement.getFlight());
    }

}
