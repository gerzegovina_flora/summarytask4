package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AutoTest.class,
        CharacteristicTest.class,
        FlightTest.class,
        StatementTest.class,
        UserTest.class
})
public class AllEntityTest {



}
