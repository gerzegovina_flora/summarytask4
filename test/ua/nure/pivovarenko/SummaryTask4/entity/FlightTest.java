package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;

import java.util.Date;

public class FlightTest {

    private Flight flight = new Flight();

    @Test
    public void testFlightConstructor(){
        new Flight();
    }

    @Before
    public void testFlightSetters(){
        flight.setAuto(new Auto());
        flight.setDescription("Description text test");
        flight.setId(16);
        flight.setFrom("Westeros");
        flight.setTo("Essos");
        flight.setDateCreate(new Date());
        flight.setFlightStatus(FlightStatus.CLOSED);
        flight.setStatement(new Statement());
    }

    @Test
    public void testFlightGetters(){
        Assert.assertTrue(flight.getFrom().equals("Westeros"));
        Assert.assertSame("Essos", flight.getTo());
        Assert.assertEquals(16, flight.getId());
        Assert.assertNotNull(flight.getDescription());
        Assert.assertSame(FlightStatus.CLOSED, flight.getFlightStatus());
        Assert.assertNotNull(flight.getStatement());
        Assert.assertNotNull(flight.getDateCreate());
        Assert.assertNotNull(flight.getAuto());
    }
}
