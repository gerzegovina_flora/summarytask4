package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

public class UserTest {

    private User user = new User();

    @Test
    public void testUserConstructor() {
        user = new User();
    }

    @Before
    public void testUserSetters() {
        user.setId(56);
        user.setLogin("lost.sawyer");
        user.setPassword("4815162342");
        user.setRole(Role.ADMIN);
        user.setIdRole(0);
    }

    @Test
    public void testUserGetters() {
        Assert.assertEquals(56, user.getId());
        Assert.assertSame("lost.sawyer", user.getLogin());
        Assert.assertSame("4815162342", user.getPassword());
        Assert.assertNotNull(user.getRole());
        Assert.assertSame(0, user.getIdRole());
    }

}
