package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;


public class CharacteristicTest {

    private Characteristic characteristic;

    @Before
    public void testCharacteristicConstructor(){
        characteristic = new Characteristic();
    }

    @Before
    public void testCharacteristicSetters(){
        characteristic.setId(8);
        characteristic.setWeightCapacity(321);
        characteristic.setWeight(123);
        characteristic.setPlaceCount(50);
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setGearType(GearType.FRONT_WHEEL);
        characteristic.setEngineType(EngineType.GAS);
    }

    @Test
    public void testCharacteristicGetters(){
        Assert.assertSame(8,characteristic.getId());
        Assert.assertSame(AutoType.MINIBUS, characteristic.getAutoType());
        Assert.assertNotEquals(EngineType.PETROL, characteristic.getEngineType());
        Assert.assertNotNull(characteristic.getGearType());
        Assert.assertTrue(characteristic.getWeightCapacity() == 321f);
        Assert.assertFalse(characteristic.getWeight() == 122f);
        Assert.assertTrue(characteristic.getPlaceCount() == 50);
    }

}
