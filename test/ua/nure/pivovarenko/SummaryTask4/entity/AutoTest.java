package ua.nure.pivovarenko.SummaryTask4.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;

public class AutoTest {

    private Auto auto = new Auto();

    @Test
    public void testAutoConstructor(){
        this.auto = new Auto();
    }

    @Before
    public void testAutoSetters(){
        auto.setId(4);
        auto.setName("User");
        auto.setAutoState(AutoState.valueOf("FREE"));
        auto.setCharacteristic(new Characteristic());
    }

    @Test
    public void testAutoGetters(){
        Assert.assertEquals("User", auto.getName());
        Assert.assertNotNull(auto.getCharacteristic());
        Assert.assertSame(4,auto.getId());
        Assert.assertSame(AutoState.FREE, auto.getAutoState());
    }

}
