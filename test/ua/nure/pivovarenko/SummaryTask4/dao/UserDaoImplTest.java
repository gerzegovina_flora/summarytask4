package ua.nure.pivovarenko.SummaryTask4.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.sql.DataSource;
import java.sql.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoImplTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
    public void testCreateUser() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        User user = mock(User.class);

        UserDaoImpl userDaoImpl = new UserDaoImpl(mockDataSource);
        //userDaoImpl.createUser(user);
    }

    @Test
    public void testUpdateUser() throws SQLException {
        User user = mock(User.class);

        when(user.getPassword()).thenReturn("4815162342");

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        UserDaoImpl userDaoImpl = new UserDaoImpl(mockDataSource);
        //userDaoImpl.updateUser(user);
    }

    @Test
    public void testGetAllUsers() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);

        when(mockResultSet.getString("status")).thenReturn(Role.ADMIN.name());

        UserDaoImpl userDaoImpl = new UserDaoImpl(mockDataSource);
        //userDaoImpl.getAllUsers();
    }

    @Test
    public void testDeleteUser() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        UserDaoImpl userDaoImpl = new UserDaoImpl(mockDataSource);
        //userDaoImpl.deleteUser(1);
    }

}
