package ua.nure.pivovarenko.SummaryTask4.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.AutoDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class AutoDaoImplTest {

    @Mock
    DataSource mockDataSource;
    @Mock
    Connection mockConn;
    @Mock
    Statement statement;
    @Mock
    PreparedStatement preparedStatement;
    @Mock
    ResultSet mockResultSet;

    @Test
    public void testGetAllAutos() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        when(mockResultSet.getString("state_name")).thenReturn("BRAKE");
        when(mockResultSet.getString("type")).thenReturn("MINIBUS");
        when(mockResultSet.getString("gear_type")).thenReturn("REAR_WHEEL");
        when(mockResultSet.getString("engine_type")).thenReturn("DIESEL");
        List<Auto> autos = autoDaoImpl.getAll();
        Assert.assertTrue(autos.size() > 0);
    }

    @Test
    public void testGetAllAutosWithException() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        when(mockResultSet.getString(anyString())).thenThrow(SQLException.class);
        autoDaoImpl.getAll();
    }

    @Test
    public void testCreateAuto() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString(), eq(PreparedStatement.RETURN_GENERATED_KEYS))).thenReturn(preparedStatement);
        doNothing().when(mockConn).commit();

        when(preparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        Auto auto = new Auto();
        auto.setAutoState(AutoState.FREE);
        Characteristic characteristic = new Characteristic();
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        characteristic.setGearType(GearType.FRONT_WHEEL);
        auto.setCharacteristic(characteristic);
        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.create(auto);
    }

    @Test
    public void testCreateAutoWithException() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString(), eq(PreparedStatement.RETURN_GENERATED_KEYS))).thenThrow(SQLException.class);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.create(new Auto());
    }

    @Test
    public void testRemoveAuto() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.delete(new Auto());
    }

    @Test
    public void testRemoveAutoWithException() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.delete(new Auto());
    }

    @Test
    public void testGetSpecifyAuto() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        Characteristic characteristic = new Characteristic();
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        characteristic.setGearType(GearType.FRONT_WHEEL);
        autoDaoImpl.getSpecifyAutos(characteristic);
    }

    @Test
    public void testGetSpecifyAutoException() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.getSpecifyAutos(new Characteristic());
    }

    @Test
    public void testUpdateAuto() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        Auto auto = new Auto();
        auto.setAutoState(AutoState.FREE);
        Characteristic characteristic = new Characteristic();
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        characteristic.setGearType(GearType.FRONT_WHEEL);
        auto.setCharacteristic(characteristic);
        autoDaoImpl.update(auto);
    }

    @Test
    public void testUpdateAutoException() throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);

        AutoDaoImpl autoDaoImpl = new AutoDaoImpl(mockDataSource);
        autoDaoImpl.update(new Auto());
    }
}
