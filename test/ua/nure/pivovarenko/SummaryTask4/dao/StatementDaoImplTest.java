package ua.nure.pivovarenko.SummaryTask4.dao;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;
import ua.nure.pivovarenko.SummaryTask4.persistence.auto.domain.Auto;
import ua.nure.pivovarenko.SummaryTask4.persistence.characteristic.domain.Characteristic;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.domain.User;

import javax.sql.DataSource;
import java.sql.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StatementDaoImplTest {
    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
    public void testAddStatement() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString(), eq(PreparedStatement.RETURN_GENERATED_KEYS))).thenReturn(preparedStatement);

        when(preparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        Characteristic characteristic = new Characteristic();
        User user = mock(User.class);
        Flight flight = mock(Flight.class);

        characteristic.setGearType(GearType.FRONT_WHEEL);
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setCharacteristic(characteristic);
        statemend.setDriver(user);
        statemend.setFlight(flight);
        statementDaoImpl.create(statemend);
    }

    @Test
    public void tesGetAllAcceptedStatements() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();

        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        when(mockResultSet.getString("gear_type")).thenReturn("FOUR_WHEEL");
        when(mockResultSet.getString("engine_type")).thenReturn("GAS");
        when(mockResultSet.getString("type")).thenReturn("MINIBUS");

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        Characteristic characteristic = new Characteristic();
        User user = mock(User.class);
        Flight flight = mock(Flight.class);

        characteristic.setGearType(GearType.FRONT_WHEEL);
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setCharacteristic(characteristic);
        statemend.setDriver(user);
        statemend.setFlight(flight);
        statementDaoImpl.getAllAcceptedStatements();
    }

    @Test
    public void testGetAllActiveStatements() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        when(mockResultSet.getString("status_name")).thenReturn("OPEN");

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        Characteristic characteristic = new Characteristic();
        User user = mock(User.class);
        Flight flight = mock(Flight.class);

        characteristic.setGearType(GearType.FRONT_WHEEL);
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setCharacteristic(characteristic);
        statemend.setDriver(user);
        statemend.setFlight(flight);
        statementDaoImpl.getAllActiveStatements(0);
    }

    @Test
    public void testAcceptStatementtatements() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        Characteristic characteristic = new Characteristic();
        Flight flight = new Flight();

        User user = mock(User.class);
        Auto auto = mock(Auto.class);

        flight.setAuto(auto);
        characteristic.setGearType(GearType.FRONT_WHEEL);
        characteristic.setAutoType(AutoType.MINIBUS);
        characteristic.setEngineType(EngineType.GAS);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setCharacteristic(characteristic);
        statemend.setDriver(user);
        statemend.setFlight(flight);
        statementDaoImpl.acceptStatement(statemend);
    }

    @Test
    public void testDeleteStatement() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        Characteristic characteristic = mock(Characteristic.class);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setCharacteristic(characteristic);
        statementDaoImpl.delete(statemend);
    }

    @Test
    public void testGetAllStatements() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);

        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockResultSet.getString("gear_type")).thenReturn("FOUR_WHEEL");
        when(mockResultSet.getString("engine_type")).thenReturn("GAS");
        when(mockResultSet.getString("type")).thenReturn("MINIBUS");

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        statementDaoImpl.getAll();
    }

    @Test
    public void testSetDoneStatement() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        Flight flight = mock(Flight.class);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setFlight(flight);

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        statementDaoImpl.setDone(statemend);
    }

    @Test
    public void testSetCancelStatement() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        Flight flight = mock(Flight.class);
        ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement statemend = new ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement();
        statemend.setFlight(flight);

        StatementDaoImpl statementDaoImpl = new StatementDaoImpl(mockDataSource);
        statementDaoImpl.setCancel(statemend);
    }

}
