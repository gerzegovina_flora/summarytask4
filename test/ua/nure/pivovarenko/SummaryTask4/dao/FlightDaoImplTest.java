package ua.nure.pivovarenko.SummaryTask4.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.FlightDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.flight.domain.Flight;

import javax.sql.DataSource;
import java.sql.*;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightDaoImplTest {

    @Mock
    DataSource mockDataSource;
    @Mock
    Connection mockConn;
    @Mock
    Statement statement;
    @Mock
    PreparedStatement preparedStatement;
    @Mock
    ResultSet mockResultSet;

    @Test
    public void testAddFlight() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(preparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        Flight flight = new Flight();
        flight.setFlightStatus(FlightStatus.CLOSED);
        flightDaoImpl.create(flight);
    }

    @Test
    public void testAddFlightException() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        flightDaoImpl.create(new Flight());
    }

    @Test
    public void testDeleteFlight() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        flightDaoImpl.delete(new Flight());
    }

    @Test
    public void testDeleteFlightException() throws SQLException {
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenThrow(SQLException.class);

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        flightDaoImpl.delete(new Flight());
    }

    @Test
    public void testUpdateFlight()throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        Flight flight = new Flight();
        flight.setFlightStatus(FlightStatus.CLOSED);
        flightDaoImpl.update(flight);
    }

    @Test
    public void testGetAllFlights()throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        when(mockResultSet.getString("status_name")).thenReturn("OPEN");

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        flightDaoImpl.getAll();
    }

    @Test
    public void testGetFlight()throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(mockResultSet.getString("status_name")).thenReturn("OPEN");

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        //flightDaoImpl.getFlight(0);
    }

    @Test
    public void testGetAffFreeFlight()throws SQLException{
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(mockResultSet.getString("status_name")).thenReturn("OPEN");

        FlightDaoImpl flightDaoImpl = new FlightDaoImpl(mockDataSource);
        flightDaoImpl.getAllFreeFlights(0);
    }
}
