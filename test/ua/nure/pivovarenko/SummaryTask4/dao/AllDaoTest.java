package ua.nure.pivovarenko.SummaryTask4.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AutoDaoImplTest.class,
        FlightDaoImplTest.class,
        StatementDaoImplTest.class,
        UserDaoImplTest.class
})
public class AllDaoTest {
}
