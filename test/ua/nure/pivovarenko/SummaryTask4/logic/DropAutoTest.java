package ua.nure.pivovarenko.SummaryTask4.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.service.auto.DropAuto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DropAutoTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private java.sql.Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
    public void testDropAuto() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("2");

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();
        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(PreparedStatement.RETURN_GENERATED_KEYS)).thenReturn(100);

        when(mockResultSet.getString("state_name")).thenReturn("BRAKE");
        when(mockResultSet.getString("type")).thenReturn("MINIBUS");
        when(mockResultSet.getString("gear_type")).thenReturn("REAR_WHEEL");
        when(mockResultSet.getString("engine_type")).thenReturn("DIESEL");

        DropAuto dropAuto = new DropAuto();

        dropAuto.execute(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testDropAutoFail() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        DropAuto dropAuto = new DropAuto();

        dropAuto.execute(httpServletRequest, httpServletResponse);
    }

}
