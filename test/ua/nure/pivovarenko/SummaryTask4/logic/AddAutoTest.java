package ua.nure.pivovarenko.SummaryTask4.logic;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoState;
import ua.nure.pivovarenko.SummaryTask4.core.constant.AutoType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.EngineType;
import ua.nure.pivovarenko.SummaryTask4.core.constant.GearType;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddAutoTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private java.sql.Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
     public void testAddAuto() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("34");
        when(httpServletRequest.getParameter("car_type")).thenReturn(AutoType.MINIBUS.name());
        when(httpServletRequest.getParameter("state")).thenReturn(AutoState.BRAKE.name());
        when(httpServletRequest.getParameter("engine")).thenReturn(EngineType.DIESEL.name());
        when(httpServletRequest.getParameter("gear")).thenReturn(GearType.FOUR_WHEEL.name());
        when(httpServletRequest.getParameter("name")).thenReturn("BMW");

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        ServletContext servletContext = mock(ServletContext.class);
        when(httpServletRequest.getServletContext()).thenReturn(servletContext);

        //AddAuto addAuto = new AddAuto(mockDataSource);

        //addAuto.execute(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testAddAutoFail() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("4");
        when(httpServletRequest.getParameter("car_type")).thenReturn(AutoType.MINIBUS.name());
        when(httpServletRequest.getParameter("state")).thenReturn(AutoState.BRAKE.name());
        when(httpServletRequest.getParameter("engine")).thenReturn(EngineType.DIESEL.name());
        when(httpServletRequest.getParameter("gear")).thenReturn(GearType.FOUR_WHEEL.name());
        when(httpServletRequest.getParameter("name")).thenReturn("4");

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString(), eq(PreparedStatement.RETURN_GENERATED_KEYS))).thenReturn(preparedStatement);
        doNothing().when(mockConn).commit();

        when(preparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        ServletContext servletContext = mock(ServletContext.class);
        when(httpServletRequest.getServletContext()).thenReturn(servletContext);

       // AddAuto addAuto = new AddAuto(mockDataSource);

        //addAuto.execute(httpServletRequest, httpServletResponse);
    }

}
