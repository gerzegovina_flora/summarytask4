package ua.nure.pivovarenko.SummaryTask4.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.Role;
import ua.nure.pivovarenko.SummaryTask4.persistence.user.UserDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.service.user.AddUser;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddUserTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private java.sql.Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
    public void testAddStatement() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        when(httpServletRequest.getParameter("role")).thenReturn(Role.DISPATCHER.name());
        when(httpServletRequest.getParameter("password")).thenReturn("malder123");
        when(httpServletRequest.getParameter("login")).thenReturn("malder123");

        ServletContext servletContext = mock(ServletContext.class);
        when(httpServletRequest.getServletContext()).thenReturn(servletContext);

        UserDaoImpl userDaoImpl = mock(UserDaoImpl.class);


        //when(userDaoImpl.checkUser(anyString())).thenReturn(null);

        when(preparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(Boolean.FALSE);
        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        AddUser addUser = new AddUser();


        addUser.execute(httpServletRequest, httpServletResponse);
    }

}
