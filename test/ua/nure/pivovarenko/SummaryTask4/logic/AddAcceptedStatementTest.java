package ua.nure.pivovarenko.SummaryTask4.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.StatementDaoImpl;
import ua.nure.pivovarenko.SummaryTask4.persistence.statement.domain.Statement;
import ua.nure.pivovarenko.SummaryTask4.service.statement.AddAcceptStatement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AddAcceptedStatementTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private java.sql.Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
     public void testAddAcceptStatement() throws ServletException, IOException, SQLException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("4");

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);
        doNothing().when(mockConn).commit();

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        StatementDaoImpl statementDaoImpl = mock(StatementDaoImpl.class);

        when(statementDaoImpl.getAll()).thenReturn(new ArrayList<Statement>());

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockDataSource.getConnection(anyString(), anyString())).thenReturn(mockConn);

        when(mockConn.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockResultSet.getString("gear_type")).thenReturn("FOUR_WHEEL");
        when(mockResultSet.getString("engine_type")).thenReturn("GAS");
        when(mockResultSet.getString("type")).thenReturn("MINIBUS");

        ServletContext servletContext = mock(ServletContext.class);
        when(httpServletRequest.getServletContext()).thenReturn(servletContext);

        AddAcceptStatement addAcceptedStatement = new AddAcceptStatement();

        addAcceptedStatement.execute(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testAddAcceptStatementFail() throws ServletException, IOException, SQLException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("sdfdsf");

        AddAcceptStatement addAcceptedStatement = new AddAcceptStatement();

        addAcceptedStatement.execute(httpServletRequest, httpServletResponse);
    }

}
