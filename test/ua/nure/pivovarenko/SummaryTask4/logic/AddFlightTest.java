package ua.nure.pivovarenko.SummaryTask4.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.nure.pivovarenko.SummaryTask4.core.constant.FlightStatus;
import ua.nure.pivovarenko.SummaryTask4.service.flight.AddFlight;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddFlightTest {

    @Mock
    private DataSource mockDataSource;
    @Mock
    private Connection mockConn;
    @Mock
    private java.sql.Statement statement;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet mockResultSet;

    @Test
    public void testAddFlight() throws SQLException, ServletException, IOException {
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

        when(httpServletRequest.getParameter(anyString())).thenReturn("value");
        when(httpServletRequest.getParameter("status")).thenReturn(FlightStatus.CLOSED.name());

        when(mockDataSource.getConnection()).thenReturn(mockConn);
        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        when(preparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);

        when(mockResultSet.next()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(mockResultSet.getInt(1)).thenReturn(2);

        when(mockConn.prepareStatement(anyString())).thenReturn(preparedStatement);

        ServletContext servletContext = mock(ServletContext.class);
        when(httpServletRequest.getServletContext()).thenReturn(servletContext);

        AddFlight addFlight = new AddFlight();

        addFlight.execute(httpServletRequest, httpServletResponse);
    }

}
