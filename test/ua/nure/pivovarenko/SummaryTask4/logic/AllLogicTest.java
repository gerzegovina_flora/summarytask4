package ua.nure.pivovarenko.SummaryTask4.logic;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AddAcceptedStatementTest.class,
        AddAutoTest.class,
        AddFlightTest.class,
        AddStatementTest.class,
        AddUserTest.class,
        CancelStatementTest.class,
        DoneStatementTest.class,
        DropAutoTest.class
})
public class AllLogicTest {
}
