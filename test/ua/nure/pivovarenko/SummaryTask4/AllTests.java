package ua.nure.pivovarenko.SummaryTask4;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.nure.pivovarenko.SummaryTask4.dao.AllDaoTest;
import ua.nure.pivovarenko.SummaryTask4.entity.AllEntityTest;
import ua.nure.pivovarenko.SummaryTask4.logic.AllLogicTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AllEntityTest.class,
        AllDaoTest.class,
        AllLogicTest.class
})
public class AllTests {



}
